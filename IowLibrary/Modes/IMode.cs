﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using IowLibrary.Devices.Model;

namespace IowLibrary.Modes {
    /// <summary>
    ///     Implementierung von Modes für den IOW. Hiermit ist es möglich beliebige Modi für den Iow abzubilden.
    ///     Jedes Device kann in einem anderen Modus betrieben werden.
    ///     Es wird immer erst die PortsInitialisation() aufgerufen.
    ///     Danach wird wenn das DataTrigger event in einem Device ausgelöst wird die RunTime gestartet und
    ///     Read() und Write() Zyklisch in jedem durchgang aufgerufen.
    ///     Um eigene Moduse zu erzeugen wird von diesem Interface abgeleitet.
    /// </summary>
    public interface IMode {
        /// <summary>
        ///     Initalisierung des IOW Bausteins. Hier können z.B. Specialmodes oder bestimmte Port Stati geladen werden.
        /// </summary>
        /// <returns>muss ein true zurück geben. Sonst wird eine Critical Event ausgelöst</returns>
        bool PortsInitialisation();

        /// <summary>
        ///     Liest den Status der Ports ein.
        /// </summary>
        /// <returns>
        ///     Eine Abbildung der Aktuellen Port Status. Dies sollte zurückgeben werden um die
        ///     weiteren Funktionen des DeviceManagers nutzen zu können. Wird im rückgabe wert eine
        ///     änderung am Port Status festgestellt wird ein DevicPortEventHandler ausgelöst.
        /// </returns>
        Dictionary<int, Port> Read();

        /// <summary>
        ///     Schreibt den aktuellen Status auf die Ports des Iow. Der übergabe Parameter
        ///     enthält den Status in den Ports des Device. Diese können genutzt werden um werte auf den Iow zu übertragen.
        /// </summary>
        /// <param name="ports"></param>
        /// <returns>muss ein true zurück geben. Sonst wird eine Critical Event ausgelöst</returns>
        bool Write(Dictionary<int, Port> ports);

        /// <summary>
        ///     Übergibt eine Instanz des Device an den Mode
        /// </summary>
        /// <param name="device"></param>
        void SetDevice(Device device);

        /// <summary>
        ///     Wird im Manager das Timeout geändert wird es hier übergeben und kann auf den Iow übertragen werden.
        /// </summary>
        /// <param name="readTimeout">in ms</param>
        /// <returns>muss ein true zurück geben. Sonst wird eine Critical Event ausgelöst</returns>
        bool ReadTimeout(int readTimeout);
    }
}