﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Threading;
using IowLibrary.Devices.Model;
using IowLibrary.IowKit;
using IowLibrary.IowKit.DllAdapter;

namespace IowLibrary.Modes {
    /// <summary>
    ///     I/O Modus für einen Iow. Dies ist ein Standart Modus im I/O funktionen mit dem Iow durchzuführen.
    /// </summary>
    public class IoMode : IMode {
        private Device _device;
        private bool _run;
        private int _timing;
        private int _writeLoopCounter;

        /// <summary>
        ///     Device set for mode will normaly call from the device at mode set.
        /// </summary>
        /// <param name="device"></param>
        public void SetDevice(Device device) {
            _device = device;
        }

        /// <summary>
        ///     Initalisation of the Ports device.
        /// </summary>
        /// <returns></returns>
        public bool PortsInitialisation() {
            var write = new byte[_device.IoReportsSize];

            write[0] = 0x00;
            write[1] = 0xff;
            write[2] = 0xff;
            var writebyts = IowKitFacade.Write(_device.Handler, Defines.PipeNumberIo, write, _device.IoReportsSize);
            if (writebyts == _device.IoReportsSize) return true;
            writebyts = IowKitFacade.Write(_device.Handler, Defines.PipeNumberIo, write, _device.IoReportsSize);
            if (writebyts == _device.IoReportsSize) return true;
            _device.AddError("Beim Device: " + _device.DeviceNumber +
                             " konnten die I/O Ports nicht Initalisiert werden.");
            return false;
        }

        /// <summary>
        ///     Reads the current state of the device ports an write it to the device Ports.
        /// </summary>
        /// <returns>device Ports</returns>
        public Dictionary<int, Port> Read() {
            var data = ReadDeviceImmediate();
            _device.SetDataStateToPort(data);
            return _device.Ports;
        }

        /// <summary>
        ///     Port IO write
        /// </summary>
        /// <param name="ports"></param>
        /// <returns></returns>
        public bool Write(Dictionary<int, Port> ports) {
            if (_run) {
                var portValue = 1;
                var data = new byte[_device.IoReportsSize];
                var counter = 2;
                while (_run) {
                    data[1] = (byte) portValue;
                    data[2] = (byte) portValue;
                    var size = IowKitFacade.Write(_device.Handler, 0, data, _device.IoReportsSize);
                    Thread.Sleep(_timing);

                    if (portValue == 1) {
                        portValue = 2;
                    } else {
                        portValue = (int) Math.Pow(2, counter);
                        counter++;
                        if (counter > 8) {
                            counter = 2;
                            portValue = 1;
                        }
                    }
                    var dataCopy = new byte[3];
                    dataCopy[0] = data[1];
                    dataCopy[1] = data[2];
                    _device.SetDataStateToPort(dataCopy);
                }
                return true;
            } else {
                var data = new byte[_device.IoReportsSize];
                data[0] = 0x00;
                foreach (var kvp in ports) {
                    var p = kvp.Value;
                    data[kvp.Key + Port.PortOffset] = p.GetBitStateAsByte();
                }

                var size = IowKitFacade.Write(_device.Handler, 0, data, _device.IoReportsSize);

                return (size != null) && (size == _device.IoReportsSize);
            }
        }

        /// <summary>
        ///     Set the ReadtimeOut to the Device
        /// </summary>
        /// <param name="readTimeout">timeout in ms</param>
        /// <returns>true on success</returns>
        public bool ReadTimeout(int readTimeout) {
            var result = IowKitFacade.Timeout(_device.Handler, readTimeout);
            if (result) return true;
            _device.AddError("Timeout konnte nicht gesetzt werden");
            return false;
        }

        /// <summary>
        ///     Lauflicht versetzt alle Ports in eine Lauflicht Funktion
        /// </summary>
        /// <param name="timing">Timinig für die lauflicht pulse</param>
        /// <param name="run">true startet das ganze im nächsten durchlauf, false stop es</param>
        /// <returns></returns>
        public bool LaufLicht(int timing, bool run) {
            _run = run;
            _timing = timing;
            _device.TriggerDataWrite();
            return true;
        }

        private byte[] ReadDeviceImmediate() {
            if (_writeLoopCounter >= 3) {
                _device.AddError("Der Versuch zu schreiben ist nach dem dritten versucht abgebrochen worden");
                return new byte[_device.IoReportsSize];
            }
            _writeLoopCounter++;
            var data = new byte[_device.IoReportsSize];
            var ok = IowKitFacade.ReadImmediate(_device.Handler, data);
            if (!ok) data = ReadDeviceImmediate();
            _writeLoopCounter = 0;
            if (data != null) return data;

            _device.AddError("Device ist offenbar nicht mehr angeschlossen");
            return new byte[_device.IoReportsSize];
        }
    }
}