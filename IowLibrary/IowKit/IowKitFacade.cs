﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Runtime.ExceptionServices;
using System.Text;
using IowLibrary.IowKit.DllAdapter;
using IowLibrary.Utils;

namespace IowLibrary.IowKit {
    /// <summary>
    ///     Facade für die IowKit Dll Adapter Funktionen
    /// </summary>
    public class IowKitFacade {
        /// <summary>
        ///     Open all devices wich are connected to the USB Ports.
        /// </summary>
        /// <returns>null if no devices was found else handler of the first device.</returns>
        public static int? OpenDevices() {
            var handler = ConvertIntPtrToInt(Method.IowKitOpenDevice());
            if (handler == 0) return null;
            return handler;
        }

        /// <summary>
        ///     Get the number of connected Devices.
        /// </summary>
        /// <returns>null if no device was found else number of Devices.</returns>
        public static int? GetConnectDeviceCounter() {
            var number = (int) Method.IowKitGetNumDevs();
            if (number == 0) return null;
            return number;
        }

        /// <summary>
        ///     Try to Close a device with the given Handler.
        /// </summary>
        /// <param name="handler">device handler</param>
        public static void CloseDevice(int? handler) {
            Method.IowKitCloseDevice(ConvertIntToIntPtr(handler));
        }

        /// <summary>
        ///     Get the handler for the Device with the number. Bevor use this you must call
        ///     OpenDevices() to open the device stream.
        /// </summary>
        /// <param name="deviceNumber">Devicenumber of the selected Device</param>
        /// <returns>null if device was not Found else handler</returns>
        /// <exception cref="IowlibraryException">If deviceNumber is null</exception>
        public static int? GetHandlerForDevice(int? deviceNumber) {
            if (deviceNumber == null) throw new IowlibraryException("devicenumber is null");

            var handler = ConvertIntPtrToInt(Method.IowKitGetDeviceHandle((uint) deviceNumber));
            if (handler == 0) return null;
            return handler;
        }

        /// <summary>
        ///     get the ProductId from a device.
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <returns>null if device could not be found else int with product id</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static int? GetProductId(int? handler) {
            if (handler == null) throw new IowlibraryException("handler to get productid is null");

            var id = Method.IowKitGetProductId(ConvertIntToIntPtr(handler));
            if (id == 0) return null;
            return (int) id;
        }

        /// <summary>
        ///     get the Prductserial from a Device.
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <returns>Productserial as String</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static string GetProductSerial(int? handler) {
            if (handler == null) throw new IowlibraryException("handler to get product serial is null");
            var sb = new StringBuilder();

            var conditon = Method.IowKitGetSerialNumber(ConvertIntToIntPtr(handler), sb);
            if (!conditon) return null;

            return sb.ToString();
        }

        /// <summary>
        ///     Get the Product Software version of the devices with the given handler
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <returns>Software Version as formated String</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static string GetProductSoftwareVersion(int? handler) {
            if (handler == null) throw new IowlibraryException("handler to get software version is null");
            var software = Method.IowKitGetRevision(ConvertIntToIntPtr(handler));
            // Es sind 4 Hex-Stellen gültig. Wäre die gegenwärtige Softwareversion 1.0.2.1 so wird
            // 0x1021 zurückgegeben.
            return software == Defines.IowNonLegacyRevision ? "no Software Version" : $"{software:X}";
        }

        /// <summary>
        ///     Read in Data from Device, while reading thread is blocked.
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <param name="numPipe">DataPipe of the usb interface</param>
        /// <param name="data">Data Buffer for Result</param>
        /// <param name="byteLength">length for the Data bytes.</param>
        /// <returns>Real number of bits wich was readin</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static int? Read(int? handler, int numPipe, byte[] data, int byteLength) {
            if (handler == null) throw new IowlibraryException("handler to to read is null");
            var handlerConvert = ConvertIntToIntPtr(handler);

            int? result = (int) Method.IowKitRead(handlerConvert, (uint) numPipe, data, (uint) byteLength);
            if (result == 0) return null;
            return result;
        }

        /// <summary>
        ///     Read in Data from Device, while reading thread is blocked.
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <param name="numPipe">DataPipe of the usb interface</param>
        /// <param name="data">Data Buffer for Result</param>
        /// <param name="byteLength">length for the Data bytes.</param>
        /// <returns>Real number of bits wich was readin</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        [HandleProcessCorruptedStateExceptions]
        public static int? IowKitReadNonBlocking(int? handler, int numPipe, ref byte[] data, int byteLength) {
            if (handler == null) throw new IowlibraryException("handler to to read is null");
            var handlerConvert = ConvertIntToIntPtr(handler);
            try {
                int? result =
                    (int) Method.IowKitReadNonBlocking(handlerConvert, (uint) numPipe, ref data[0], (uint) byteLength);
                // 0 bedeutet fehler oder keine änderungen im Stack
                if (result == 0) return null;
                return result;
            } catch (AccessViolationException) {
                throw new IowlibraryException(
                    "AccessViolationException wurde von der iowkit.dll geworfen. Dies ist ein zeichen dafür das die Verbindung zum iow verloren wurde. Die verbidung wird Automatisch getrennt.");
            } catch (DivideByZeroException) {
                throw new IowlibraryException(
                    "AccessViolationException wurde von der iowkit.dll geworfen. Dies ist ein zeichen dafür das die Verbindung zum iow verloren wurde. Die verbidung wird Automatisch getrennt.");
            }
        }

        /// <summary>
        ///     Reads ar Device Immediat
        /// </summary>
        /// <param name="handler">handler</param>
        /// <param name="data">report object for the result</param>
        /// <returns>true if read was valid</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static bool ReadImmediate(int? handler, byte[] data) {
            if (handler == null) throw new IowlibraryException("handler to to read is null");
            var handlerConvert = ConvertIntToIntPtr(handler);

            return Method.IowKitReadImmediate(handlerConvert, data);
        }

        /// <summary>
        ///     Write Data to a Device.
        /// </summary>
        /// <param name="handler">Handler for the Device</param>
        /// <param name="numPipe">DataPipe of the usb interface</param>
        /// <param name="data">data wich are write to the device</param>
        /// <param name="byteLength">lenght of the Data bytes</param>
        /// <returns>Real number of bits wich was write to the device</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static int? Write(int? handler, int numPipe, byte[] data, int byteLength) {
            if (handler == null) throw new IowlibraryException("handler to to write is null");
            var handlerConvert = ConvertIntToIntPtr(handler);

            int? result = (int) Method.IowKitWrite(handlerConvert, (uint) numPipe, data, (uint) byteLength);
            if (result == 0) result = null;

            return result;
        }

        /// <summary>
        ///     Set Timeout Read/Write time for the device
        /// </summary>
        /// <param name="handler">valid device handler</param>
        /// <param name="timeout">time in ms</param>
        /// <returns>True if set was valid</returns>
        /// <exception cref="IowlibraryException">If handler is null</exception>
        public static bool Timeout(int? handler, int timeout) {
            if (handler == null) throw new IowlibraryException("handler to to write is null");
            var handlerConvert = ConvertIntToIntPtr(handler);
            return Method.IowKitSetTimeout(handlerConvert, (uint) timeout);
        }

        private static IntPtr ConvertIntToIntPtr(int? input) {
            if (input == null) throw new IowlibraryException("input to convert In to IntPtr is null");
            return (IntPtr) input;
        }

        private static int ConvertIntPtrToInt(IntPtr input) {
            return (int) input;
        }
    }
}