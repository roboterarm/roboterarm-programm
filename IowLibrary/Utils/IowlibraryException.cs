﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;

namespace IowLibrary.Utils {
    /// <summary>
    ///     Exception die von der Library ausgelöst werden können
    /// </summary>
    public class IowlibraryException : Exception {
        /// <summary>
        ///     IowLibrary Exception
        /// </summary>
        public IowlibraryException() {
        }

        /// <summary>
        ///     IowLibrary Exception
        /// </summary>
        /// <param name="msg">string msg</param>
        public IowlibraryException(string msg) : base(msg) {
        }

        /// <summary>
        ///     IowLibrary Exception
        /// </summary>
        /// <param name="msg">string msg</param>
        /// <param name="e">exception to warp</param>
        public IowlibraryException(string msg, Exception e) : base(msg, e) {
        }
    }
}