﻿using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("IowLibrary")]
[assembly: AssemblyDescription("Library zum ansteuern des IO Warrior24.  https://tldrlegal.com/license/mit-license")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("www.edlly.de")]
[assembly: AssemblyProduct("IowLibrary")]
[assembly: AssemblyCopyright("www.edlly.de")]
[assembly: AssemblyTrademark("Copyright 2017 C. Metzner M. Vervoorst")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("c2606d6f-db64-4ba3-9139-be9e63139ad8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]

[assembly: AssemblyVersion("2.0.0.0")]
[assembly: AssemblyFileVersion("2.0.0.0")]
[assembly: NeutralResourcesLanguage("de-DE", UltimateResourceFallbackLocation.MainAssembly)]