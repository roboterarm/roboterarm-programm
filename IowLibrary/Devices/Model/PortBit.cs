﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Timers;
using IowLibrary.IowKit.DllAdapter;

namespace IowLibrary.Devices.Model {
    /// <summary>
    ///     Bit change event delegater
    /// </summary>
    /// <param name="portbit"></param>
    public delegate void PortBitChangeEventHandler(PortBit portbit);

    /// <summary>
    ///     Abblindung eines Bits in einem Device
    /// </summary>
    public class PortBit {
        private const int DeboundeTime = 10;

        private readonly Timer _debounceTimer;
        private bool _bitIn;
        private bool _bitInDebounced;


        private int _bitNumber;
        private bool _bitOut;

        /// <summary>
        ///     instance of new bitNumber
        /// </summary>
        /// <param name="bitNumber">bit number</param>
        public PortBit(int bitNumber) {
            _bitNumber = bitNumber;
            _debounceTimer = new Timer(DeboundeTime);
            _debounceTimer.Elapsed += DebounceTimer_Elapsed;
        }

        /// <summary>
        ///     Output Bit
        /// </summary>
        public bool BitOut {
            get { return _bitOut; }
            set {
                _bitOut = value;
                ChangeOutEvent();
            }
        }

        /// <summary>
        ///     Input Bit
        /// </summary>
        public bool BitIn {
            get { return _bitIn; }
            set {
                if (_bitIn == value) return;
                _bitIn = value;
                Debouncing();
            }
        }

        /// <summary>
        ///     Bit Number
        /// </summary>
        /// <exception cref="IndexOutOfRangeException">if number is greater than MaxBitNumber</exception>
        public int BitNumber {
            get { return _bitNumber; }
            set {
                if (value < Defines.MaxBitNumber)
                    throw new IndexOutOfRangeException("Max Bitnumber is: " + Defines.MaxBitNumber + " given was: " +
                                                       value);
                _bitNumber = value;
            }
        }

        /// <summary>
        ///     reports output bit changes
        /// </summary>
        public event PortBitChangeEventHandler ChangeOut;

        /// <summary>
        ///     report input bit changes
        /// </summary>
        public event PortBitChangeEventHandler ChangeIn;

        /// <summary>
        ///     Converts boolean to int true = 1 false = 0
        /// </summary>
        /// <param name="value">true or false</param>
        /// <returns>1 or 0</returns>
        public static int ConvertToInt(bool value) {
            return value ? 1 : 0;
        }

        private void ChangeOutEvent() {
            ChangeOut?.Invoke(this);
        }

        private void ChangeInEvent() {
            ChangeIn?.Invoke(this);
        }

        private void Debouncing() {
            if (_debounceTimer.Enabled) return;

            _debounceTimer.Enabled = true;
            _debounceTimer.Start();
            _bitInDebounced = _bitIn;
        }

        private void DebounceTimer_Elapsed(object sender, ElapsedEventArgs e) {
            if (_bitIn == _bitInDebounced) ChangeInEvent();
            try {
                _debounceTimer.Stop();
                _debounceTimer.Enabled = false;
            }
            catch (NullReferenceException) {
                // ignore
                _debounceTimer.Enabled = false;
            }
        }
    }
}