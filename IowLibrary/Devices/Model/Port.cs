﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using IowLibrary.IowKit.DllAdapter;
using IowLibrary.Utils;

namespace IowLibrary.Devices.Model {
    /// <summary>
    ///     reports port events
    /// </summary>
    /// <param name="port">this</param>
    public delegate void PortEventHandler(Port port);

    /// <summary>
    ///     reports port change events
    /// </summary>
    /// <param name="port">this</param>
    /// <param name="portBit">bit causer</param>
    public delegate void PortChangeEventHandler(Port port, PortBit portBit);


    /// <summary>
    ///     Abbildung eines Ports in einem Device
    /// </summary>
    public class Port {
        /// <summary>
        ///     offset for the port result list
        /// </summary>
        public const int PortOffset = 1;

        /// <summary>
        ///     Init a port an generate bit objects
        /// </summary>
        /// <param name="portNumber">Number of the Port</param>
        public Port(int portNumber) {
            PortNumber = portNumber;
            InitPort();
        }

        /// <summary>
        ///     Bits in the Port
        /// </summary>
        public List<PortBit> PortBits { get; set; }

        /// <summary>
        ///     Individual port number
        /// </summary>
        public int PortNumber { get; set; }

        /// <summary>
        ///     reports bit on change event
        /// </summary>
        public event PortChangeEventHandler PortBitInChange;

        /// <summary>
        ///     report bit out change event
        /// </summary>
        public event PortChangeEventHandler PortBitOutChange;

        /// <summary>
        ///     set a bit of this port to the given state
        /// </summary>
        /// <param name="bit">number of the bit</param>
        /// <param name="value">state</param>
        public void SetBit(int bit, bool value) {
            if (PortBits.Count - 1 < bit) return;

            var pb = PortBits[bit];
            pb.BitOut = value;
        }

        /// <summary>
        ///     Returns the State of the out bits as Byte
        /// </summary>
        /// <returns>state of out bit as byte</returns>
        public byte GetBitStateAsByte() {
            byte portMaskState = 0x00;
            foreach (var bit in PortBits) {
                var value = PortBit.ConvertToInt(bit.BitOut);
                var bitNum = bit.BitNumber;

                portMaskState |= (byte) (value << bitNum);
            }
            return portMaskState;
        }

        /// <summary>
        ///     set a byte to in bits
        /// </summary>
        /// <param name="dataIn">byte which will set to bits</param>
        public void SetBitState(byte dataIn) {
            foreach (var bit in PortBits) {
                var mask = (byte) (1 << bit.BitNumber);
                bit.BitIn = (dataIn & mask) == mask;
            }
        }

        /// <summary>
        ///     Returns the Bit State
        /// </summary>
        /// <param name="bit">Bit State</param>
        /// <returns>Den Status des ausgewählten Bits</returns>
        /// <exception cref="IowlibraryException">Wenn das gegebene Bit nicht im gültigen Bereich liegt.</exception>
        public bool GetBitState(int bit) {
            if (PortBits.Count < bit + 1) throw new IowlibraryException("Bits " + bit + " ausserhalb des Bereichs");
            return PortBits[bit].BitIn;
        }

        private void InitPort() {
            if (PortBits == null) PortBits = new List<PortBit>();

            for (var i = 0; i < Defines.MaxBitNumber + 1; i++) {
                var bit = new PortBit(i);
                bit.ChangeIn += BitInChange;
                bit.ChangeOut += BitOutChange;
                PortBits.Add(bit);
            }
        }

        private void BitInChange(PortBit portbit) {
            PortBitInChangeEvent(portbit);
        }

        private void BitOutChange(PortBit portbit) {
            PortBitOutChangeEvent(portbit);
        }

        private void PortBitInChangeEvent(PortBit portbit) {
            PortBitInChange?.Invoke(this, portbit);
        }

        private void PortBitOutChangeEvent(PortBit portbit) {
            PortBitOutChange?.Invoke(this, portbit);
        }
    }
}