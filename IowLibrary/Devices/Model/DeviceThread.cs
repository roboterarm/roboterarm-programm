﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using IowLibrary.IowKit.DllAdapter;

namespace IowLibrary.Devices.Model {
    /// <summary>
    ///     Delegates Runtimes events
    /// </summary>
    /// <param name="runtime">Runtime in ms</param>
    public delegate void DeviceThreadRuntimeEvent(long runtime);

    /// <summary>
    ///     Abbildung eines DeviceThread
    /// </summary>
    public class DeviceThread {
        private const int CountRuntimeRounds = 50;
        private readonly Stopwatch _stopwatch = new Stopwatch();
        private readonly List<long> _stopWatchResult = new List<long>();
        private bool _isDataWrite;

        private long _runtime;

        private volatile bool _stopHandler;

        /// <summary>
        ///     Holds Methode for the Device to handel IO Loop
        /// </summary>
        /// <param name="device">device wich will be threaded</param>
        public DeviceThread(Device device) {
            Device = device;
            if (Device != null) {
                Device.PortBitOutChange += Device_PortBitOutChange;
                Device.TriggerData += Device_TriggerData;
            }
        }

        /// <summary>
        ///     the device Thread
        /// </summary>
        public Thread DeviceThreadInstance { get; set; }

        /// <summary>
        ///     is set with the param ctor
        /// </summary>
        public Device Device { get; set; }

        /// <summary>
        ///     Runtime of the this device thread
        /// </summary>
        public long Runtime {
            get { return _runtime; }
            set {
                _runtime = value;
                RunTimeUpdate?.Invoke(_runtime);
            }
        }

        /// <summary>
        ///     When a new Runtime measure result is available this event will it report
        /// </summary>
        public event DeviceThreadRuntimeEvent RunTimeUpdate;

        /// <summary>
        ///     Starts the Device I/O Loop
        /// </summary>
        public void RunDevice() {
            _stopHandler = false;

            Device.Mode.PortsInitialisation();

            Device.Mode.ReadTimeout(Defines.DeviceDefaultTimeout);

            while (!_stopHandler) {
                _stopwatch.Reset();
                _stopwatch.Start();
                if (_isDataWrite) {
                    var ok = Device.Mode.Write(Device.Ports);
                    if (ok) _isDataWrite = false;
                }

                Device.Mode.Read();
                _stopwatch.Stop();
                AddStopWatchResult(_stopwatch.ElapsedMilliseconds);
            }
        }

        /// <summary>
        ///     Stops the I/O loop at the next cycle
        /// </summary>
        public void RequestStop() {
            _stopHandler = true;
        }

        private void AddStopWatchResult(long stopwatchElapsedMilliseconds) {
            _stopWatchResult.Add(stopwatchElapsedMilliseconds);

            if (_stopWatchResult.Count() < CountRuntimeRounds) return;

            UpdateRuntime();
        }

        private void UpdateRuntime() {
            var time = _stopWatchResult.Sum();
            time = time/_stopWatchResult.Count();
            Runtime = time;
            _stopWatchResult.Clear();
        }

        private void Device_PortBitOutChange(Device device, Port port, PortBit portbit) {
            _isDataWrite = true;
        }

        private void Device_TriggerData(Device device) {
            _isDataWrite = true;
        }
    }
}