﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;

namespace IowLibrary.Devices.Model {
    /// <summary>
    ///     Datenklassen Abbildung fuer einen LogEintrag
    /// </summary>
    public class LogEntry {
        /// <summary>
        ///     Level of Logs
        /// </summary>
        public enum LogLevel {
            /// <summary>
            ///     Info Level
            /// </summary>
            Info = 0,

            /// <summary>
            ///     Event Level
            /// </summary>
            Event = 1,


            /// <summary>
            ///     Error Level
            /// </summary>
            Error = 2,

            /// <summary>
            ///     Error Level
            /// </summary>
            Critical = 3
        }

        private LogEntry(Device device, string message, LogLevel level) {
            Message = message;
            Device = device;
            TimeStamp = DateTime.Now;
            Level = level;
        }

        /// <summary>
        ///     Log Message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        ///     initiator device
        /// </summary>
        public Device Device { get; set; }

        /// <summary>
        ///     timestamp of even
        /// </summary>
        public DateTime TimeStamp { get; set; }

        /// <summary>
        ///     level of the event
        /// </summary>
        public LogLevel Level { get; set; }

        /// <summary>
        ///     String Rückgabe des Levels
        /// </summary>
        public string StringLevel {
            get {
                switch (Level) {
                    case LogLevel.Info:
                        return "Info";
                    case LogLevel.Event:
                        return "Event";
                    case LogLevel.Error:
                        return "Fehler";
                    default:
                        return "Kritisch";
                }
            }
        }


        /// <summary>
        ///     Generates a New Instance of LogEntry with given paramertes.
        ///     Set the timestamp to the current timestamp
        /// </summary>
        /// <param name="device">initiator device </param>
        /// <param name="message">Log Message</param>
        /// <param name="level">level of the event</param>
        /// <returns>new LogEntry instance</returns>
        public static LogEntry NewLogEntry(Device device, string message, LogLevel level) {
            return new LogEntry(device, message, level);
        }

        /// <summary>
        ///     Converts Object to String
        /// </summary>
        /// <returns>String representation of object</returns>
        public override string ToString() {
            if (Device == null) return TimeStamp + " - " + Message;
            return TimeStamp + "Device: " + Device.DeviceNumber + " - " + " - " + Message;
        }
    }
}