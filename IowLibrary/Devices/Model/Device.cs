﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using IowLibrary.Devices.Control;
using IowLibrary.IowKit;
using IowLibrary.IowKit.DllAdapter;
using IowLibrary.Modes;

namespace IowLibrary.Devices.Model {
    /// <summary>
    ///     Fuer Statusaenderungen am Device
    /// </summary>
    /// <param name="device">this</param>
    public delegate void DeviceStatusEventHandler(Device device);

    /// <summary>
    ///     Fuer aenderung am Port bzw. Bit Status des Devices
    /// </summary>
    /// <param name="device">this</param>
    /// <param name="port">Port which has the event</param>
    /// <param name="portBit">Bit which has the event</param>
    public delegate void DevicPortEventHandler(Device device, Port port, PortBit portBit);

    /// <summary>
    ///     Abbildung eines Iow Bausteins als Klasse
    /// </summary>
    public class Device {
        private IMode _mode;

        /// <summary>
        ///     Paramtered Ctor for an device. It will Init the device with the given handler.
        /// </summary>
        /// <param name="handler">handler of the device to startup</param>
        public Device(int? handler) {
            Handler = handler;
            DeviceInitialisation();
        }

        /// <summary>
        ///     Logging for device
        /// </summary>
        public Log Log { get; set; } = new Log();

        /// <summary>
        ///     individual device handler from the iow driver
        /// </summary>
        public int? Handler { get; set; }

        /// <summary>
        ///     individual deviceNumber from the iow driver
        /// </summary>
        public int DeviceNumber { get; set; }

        /// <summary>
        ///     will be readout at initialization of the device
        /// </summary>
        public int? ProductId { get; set; }

        /// <summary>
        ///     will be readout at initialization of the device
        /// </summary>
        public string Serial { get; set; }

        /// <summary>
        ///     will be readout at initialization of the device
        /// </summary>
        public string SoftwareVersion { get; set; }

        /// <summary>
        ///     Contains the Ports which will be generated on device initialization
        /// </summary>
        public Dictionary<int, Port> Ports { get; set; }

        /// <summary>
        ///     will be set automatical at initialization of the device
        /// </summary>
        public int ReportSize { get; set; }

        /// <summary>
        ///     will be set automatical at initialization of the device
        /// </summary>
        public int IoReportsSize { get; set; }

        /// <summary>
        ///     Mode for the device.
        /// </summary>
        public IMode Mode {
            get { return _mode; }
            set {
                _mode = value;
                _mode.SetDevice(this);
            }
        }

        /// <summary>
        ///     Wird beim Schliessen des Devices aufgerufen
        /// </summary>
        public event DeviceStatusEventHandler DeviceClose;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Info
        /// </summary>
        public event DeviceStatusEventHandler InfoLog;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Event
        /// </summary>
        public event DeviceStatusEventHandler EventLog;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Error
        /// </summary>
        public event DeviceStatusEventHandler ErrorLog;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Critical
        /// </summary>
        public event DeviceStatusEventHandler CriticalLog;

        /// <summary>
        ///     Wenn sich ein Input Bit geaendert hat
        /// </summary>
        public event DevicPortEventHandler PortBitInChange;

        /// <summary>
        ///     Wenn sich ein Output Bit geaendert hat
        /// </summary>
        public event DevicPortEventHandler PortBitOutChange;

        /// <summary>
        ///     Wenn datenanederung durchgefueht werden sollen
        /// </summary>
        public event DeviceStatusEventHandler TriggerData;

        /// <inheritdoc />
        ~Device() {
            Close();
        }

        /// <summary>
        ///     Set of a Bit on a Port of this Devices
        /// </summary>
        /// <param name="port">Ports to set Starts with 0</param>
        /// <param name="bit">Bit to set starts with 0</param>
        /// <param name="value">target state</param>
        public void SetBit(int port, int bit, bool value) {
            if (Ports.Count - 1 < port) return;
            var p = Ports[port];
            p.SetBit(bit, value);
        }

        /// <summary>
        ///     Set all bit of a ports to a value
        /// </summary>
        /// <param name="port">port number</param>
        /// <param name="value">ture or false</param>
        public void SetAllPortBits(int port, bool value) {
            for (var bit = 0; bit < Defines.MaxBitNumber + 1; bit++) SetBit(port, bit, value);
        }

        /// <summary>
        ///     Close this Device. Triggers the Devices close Event
        /// </summary>
        public void Close() {
            IowKitFacade.CloseDevice(Handler);
            DeviceCloseEvent();
        }

        /// <summary>
        ///     Write the Current out state of the ports to the Device
        /// </summary>
        /// <returns>true if all Bits write to the device</returns>
        public bool WritePortStateToDevice() {
            var data = new byte[IoReportsSize];
            data[0] = 0x00;
            foreach (var kvp in Ports) {
                var p = kvp.Value;
                data[kvp.Key + Port.PortOffset] = p.GetBitStateAsByte();
            }

            var size = IowKitFacade.Write(Handler, 0, data, IoReportsSize);

            return (size != null) && (size == IoReportsSize);
        }

        /// <summary>
        ///     Set a ArrayList of byte Data Reports to the Device
        /// </summary>
        /// <param name="data">Byte of data für ports</param>
        public void SetDataStateToPort(IEnumerable<byte> data) {
            // Da wir mit readImm arbeiten ist das result byte 0 das erste
            var i = 0;
            foreach (var dataIn in data) {
                if (i + 1 <= Ports.Count) {
                    var port = Ports[i];
                    port.SetBitState(dataIn);
                } else {
                    break;
                }
                i++;
            }
        }

        /// <summary>
        ///     Set a ArrayList of byte Data Reports to the Device
        /// </summary>
        /// <param name="data">Byte of data für ports</param>
        public void SetDataReadStateToPort(IEnumerable<byte> data) {
            // Da wir mit Read arbeiten ist das result byte 1 das erste
            var i = 0;
            foreach (var dataIn in data) {
                if (i == 0) {
                    i++;
                    continue;
                }

                if (i <= Ports.Count) {
                    var port = Ports[i - 1];
                    port.SetBitState(dataIn);
                } else {
                    break;
                }
                i++;
            }
        }

        /// <summary>
        ///     Trigger for DataWrite of device Threadhandler
        /// </summary>
        public void TriggerDataWrite() {
            TriggerData?.Invoke(this);
        }

        private void DeviceInitialisation() {
            ReadDeviceProductId();
            ReadDeviceSerial();
            ReadSoftwareVersion();
            PortsInitialisation();
        }

        private void ReadDeviceProductId() {
            if (ProductId == null) {
                ProductId = IowKitFacade.GetProductId(Handler);
                if (ProductId == null) AddCritical("Fehler beim Lesen der Produkt Id des Devices");
            }
            SetDeviceReportSize();
        }

        private void SetDeviceReportSize() {
            switch (ProductId) {
                case Defines.PidIow24:
                    ReportSize = Defines.ReportSize;
                    IoReportsSize = Defines.IoReportSize;
                    break;
                default:
                    ReportSize = Defines.SpecialReportSize;
                    IoReportsSize = Defines.SpecialReportSize;
                    break;
            }
        }

        private void ReadDeviceSerial() {
            Serial = IowKitFacade.GetProductSerial(Handler);
        }

        private void ReadSoftwareVersion() {
            SoftwareVersion = IowKitFacade.GetProductSoftwareVersion(Handler);
        }

        private void PortsInitialisation() {
            if (Ports == null) Ports = new Dictionary<int, Port>();
            for (var i = 0; i < 2; i++) {
                var port = new Port(i);
                port.PortBitInChange += PortBitInChangeEvent;
                port.PortBitOutChange += PortBitOutChangeEvent;
                Ports.Add(i, port);
            }
        }

        private void PortBitOutChangeEvent(Port port, PortBit portBit) {
            PortBitOutChange?.Invoke(this, port, portBit);
            AddInfoLog("Out Port: " + port.PortNumber + "."
                       + portBit.BitNumber + " verändert zu: " + portBit.BitOut);
        }

        private void PortBitInChangeEvent(Port port, PortBit portBit) {
            PortBitInChange?.Invoke(this, port, portBit);
            AddInfoLog("In Port: " + port.PortNumber + "."
                       + portBit.BitNumber + " verändert zu: " + portBit.BitIn);
        }

        private void DeviceCloseEvent() {
            DeviceClose?.Invoke(this);
            AddEventLog("wurde erfolgreich geschlossen.");
        }

        /// <summary>
        ///     Adds Device Info Log
        /// </summary>
        /// <param name="log">Event to Add</param>
        public void AddInfoLog(string log) {
            Log.AddInfoLog(this, log);
            InfoLog?.Invoke(this);
        }

        /// <summary>
        ///     Adds Device Event Log
        /// </summary>
        /// <param name="log">Event to Add</param>
        public void AddEventLog(string log) {
            Log.AddEventLog(this, log);
            EventLog?.Invoke(this);
        }

        /// <summary>
        ///     Adds Device Error Log
        /// </summary>
        /// <param name="log">Event to Add</param>
        public void AddError(string log) {
            Log.AddErrorLog(this, log);
            ErrorLog?.Invoke(this);
        }

        /// <summary>
        ///     Add an error to the device error log
        ///     ATTENTION: THIS STOPS THE DEVICE AND DISCONNECT IT!!!
        /// </summary>
        /// <param name="msg">error log entry</param>
        public void AddCritical(string msg) {
            Log.AddCriticalLog(this, msg);
            if (CriticalLog == null) throw new SystemException("Error at Devices handling: " + msg);
            CriticalLog(this);
        }
    }
}