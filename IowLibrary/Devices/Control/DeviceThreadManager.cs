﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Threading;
using IowLibrary.Devices.Model;

namespace IowLibrary.Devices.Control {
    /// <summary>
    ///     Verwaltet die Threads fuer die Devices
    /// </summary>
    public class DeviceThreadManager {
        private readonly Dictionary<int, DeviceThread> _deviceThreadpool =
            new Dictionary<int, DeviceThread>();

        /// <inheritdoc />
        ~DeviceThreadManager() {
            StopAllDeviceThreads();
        }

        /// <summary>
        ///     Startet einen Thread fuer einen Devices und fuegt im dem Pool hinzu.
        /// </summary>
        /// <param name="device"></param>
        /// <returns>null if device is null else the new DeviceThreadInstance object</returns>
        public DeviceThread AddNewDeviceThread(Device device) {
            if (device == null) return null;
            var deviceHandler = new DeviceThread(device);
            var thread = new Thread(deviceHandler.RunDevice);
            deviceHandler.DeviceThreadInstance = thread;
            thread.Start();
            if (thread.IsAlive) {
                if (_deviceThreadpool.ContainsKey(device.DeviceNumber)) StopDeviceThread(device);
                _deviceThreadpool.Add(device.DeviceNumber, deviceHandler);
            }
            return deviceHandler;
        }

        /// <summary>
        ///     Stop einen Thread fuer ein Device und entfernt ihn vom Pool
        /// </summary>
        /// <param name="device"></param>
        public void StopDeviceThread(Device device) {
            if (device == null) return;
            DeviceThread deviceThread;
            _deviceThreadpool.TryGetValue(device.DeviceNumber, out deviceThread);
            deviceThread?.RequestStop();
            _deviceThreadpool.Remove(device.DeviceNumber);
        }

        /// <summary>
        ///     Stoppt alle Thread und entfernt alles
        /// </summary>
        public void StopAllDeviceThreads() {
            foreach (var deviceEntry in _deviceThreadpool) {
                var deviceHandler = deviceEntry.Value;
                var thread = deviceHandler.DeviceThreadInstance;
                deviceHandler.RequestStop();
                thread.Join();
            }
            _deviceThreadpool.Clear();
        }
    }
}