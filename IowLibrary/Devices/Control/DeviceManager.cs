﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using IowLibrary.Devices.Model;
using IowLibrary.IowKit;
using IowLibrary.IowKit.DllAdapter;
using IowLibrary.Modes;
using IowLibrary.Utils;

namespace IowLibrary.Devices.Control {
    /// <summary>
    ///     Events die vom Manager ausgelöst und weiter gereicht werden
    /// </summary>
    /// <param name="device">instanz von DeviceManager</param>
    public delegate void ManagerEventHandler(DeviceManager device);

    /// <summary>
    ///     Event das von der Runtime aufgerüfen wird es enhält die Aktuelle Laufzeit eines Devices
    /// </summary>
    /// <param name="runtime">runtime in ms</param>
    public delegate void DeviceRuntimeEventHandler(long runtime);

    /// <summary>
    ///     Öffnet all Devices und Verwaltet diese. Es schließt diese bei bedarf auch und kümmert sich um die Fehler
    ///     behandlung.
    ///     Im Fehlerfall eines CriticalErrors z.B. das abziehen des USB Steckers wird die Factory zurück gesetzt.
    ///     <code>
    ///  // Öffnen eines neuen Managers
    ///  // Übergeben werden hier Methoden die ManagerEventHandler behandeln
    /// _deviceFactory = new DeviceManager( infoError,  deviceEvent, deviceError,  critical);
    /// // Verbindung aufbauen und Initalisieren
    /// var isConnected = _deviceFactory.InitFactory();
    /// 
    /// // Starten eine I/O Mode auf das gewählte Device
    ///  int selectedDevice = 1;
    ///  _deviceFactory.RunDevice(selectedDevice, Device_PortChangeStatus, DeviceFactoryRunTimeUpdate);
    /// 
    /// // Stoppen alles vorgänge und entfernen alles Devices
    /// _deviceFactory.RemoveAllDevices();
    /// </code>
    /// </summary>
    public class DeviceManager {
        private readonly DeviceManager _instance;

        private int? _deviceCounter;
        private DeviceThreadManager _deviceThreadManager = new DeviceThreadManager();

        /// <summary>
        ///     Einstiegspunkt in für einen neuen Manager
        /// </summary>
        /// <param name="deviceError"></param>
        /// <param name="infoError"></param>
        /// <param name="deviceEvent"></param>
        /// <param name="critical"></param>
        public DeviceManager(ManagerEventHandler infoError, ManagerEventHandler deviceEvent,
            ManagerEventHandler deviceError, ManagerEventHandler critical) {
            DeviceInfo += infoError;
            DeviceEvent += deviceEvent;
            DeviceError += deviceError;
            DeviceCritical += critical;

            if (_instance == null) _instance = this;
        }

        /// <summary>
        ///     Anmeldete Devices am Manager
        /// </summary>
        public Dictionary<int, Device> Devices { get; set; }

        /// <summary>
        ///     Logger for Event, Errors...
        /// </summary>
        public Log Log { get; set; } = Log.NewInstance();

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Info
        /// </summary>
        public event ManagerEventHandler DeviceInfo;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Event
        /// </summary>
        public event ManagerEventHandler DeviceEvent;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Error
        /// </summary>
        public event ManagerEventHandler DeviceError;

        /// <summary>
        ///     Weiterleiten von Events mit Stufe Critical
        /// </summary>
        public event ManagerEventHandler DeviceCritical;

        /// <summary>
        ///     Weiterleiten eines neuen Zykluszeit einer Laufenden Runtime
        /// </summary>
        public event DeviceRuntimeEventHandler RunTimeUpdate;

        /// <inheritdoc />
        ~DeviceManager() {
            RemoveAllDevices();
        }

        /// <summary>
        ///     Initialisierung des Managers Öffnet alle Verbundenen Devices und registriert diese im Manager
        /// </summary>
        /// <returns>im Fehlerfall ein false sonst ein true</returns>
        public bool InitFactory() {
            if (Devices != null) RemoveAllDevices();

            var isOpen = OpenConnectedDevices();
            if (!isOpen) return false;

            var isCountDevices = CountConnectedDevices();
            if (!isCountDevices) return false;
            return AddAllConnectedDeviceToFactory();
        }

        /// <summary>
        ///     Startet die Runtime für das gegebene Device. Dabei wird der Mode welcher übergeben worden ist genutzt.
        /// </summary>
        /// <param name="deviceNumber"></param>
        /// <param name="devicePortBitChange">PortBit Change Event Listener</param>
        /// <param name="deviceFactoryRunTimeRuntime">Loop Timer Update Event</param>
        /// <param name="mode">ein von IMode abgeleiter Modus</param>
        public void RunDevice(object deviceNumber, DevicPortEventHandler devicePortBitChange,
            DeviceRuntimeEventHandler deviceFactoryRunTimeRuntime, IMode mode) {
            var device = GetDeviceNumber(deviceNumber);
            if (device == null) return;
            device.PortBitInChange += devicePortBitChange;
            RunTimeUpdate += deviceFactoryRunTimeRuntime;
            RunDevice(device.DeviceNumber, mode);
        }

        /// <summary>
        ///     Startet die Runtime für das gegebene Device. Dabei wird der Mode welcher übergeben worden ist genutzt
        /// </summary>
        /// <param name="deviceNumber"></param>
        /// <param name="mode">ein von IMode abgeleiter Modus</param>
        public void RunDevice(int deviceNumber, IMode mode) {
            var device = GetDeviceNumber(deviceNumber);
            device.Mode = mode;
            if (_deviceThreadManager == null) _deviceThreadManager = new DeviceThreadManager();
            var deviceHandler = _deviceThreadManager.AddNewDeviceThread(device);
            if (deviceHandler == null) return;
            deviceHandler.RunTimeUpdate += DeviceHandler_RunTimeUpdate;
        }

        /// <summary>
        ///     Startet die Runtime für das gegebene Device. Dabei wird der Mode welcher übergeben worden ist genutzt
        /// </summary>
        /// <param name="deviceNumber"></param>
        /// <param name="mode">ein von IMode abgeleiter Modus</param>
        public void RunDeviceFromSenderObject(object deviceNumber, IMode mode) {
            try {
                var selectedDevice = Convert.ToInt32(deviceNumber);
                RunDevice(selectedDevice, mode);
            } catch (Exception) {
                AddCritical("Es gabe einen Fehler beim Versuch das Device zu Starten.");
            }
        }

        /// <summary>
        ///     Stop die Runtime für ein Devices
        /// </summary>
        /// <param name="deviceNumber"></param>
        public void StopDevice(int deviceNumber) {
            var device = GetDeviceNumber(deviceNumber);
            _deviceThreadManager?.StopDeviceThread(device);
        }

        /// <summary>
        ///     Stop die Runtime für ein Devices
        /// </summary>
        /// <param name="deviceNumber"></param>
        public void StopDevice(object deviceNumber) {
            try {
                int selectedDevice;
                if (deviceNumber is Device) selectedDevice = ((Device) deviceNumber).DeviceNumber;
                else selectedDevice = Convert.ToInt32(deviceNumber);
                StopDevice(selectedDevice);
            } catch (Exception) {
                AddCritical(
                    "Aus dem gegebenen Objekt konnte kein gültiges Gerät ermittelt werden. " +
                    "Der vorgang wurde abgebrochen.");
            }
        }

        /// <summary>
        ///     Setzt ein Bit im angeben Port eines Devices
        /// </summary>
        /// <param name="deviceNumber">e</param>
        /// <param name="port">Port ist 0 basierend</param>
        /// <param name="bit">Bit ist 0 basierend</param>
        /// <param name="value">true oder false</param>
        public void SetBit(int deviceNumber, int port, int bit, bool value) {
            var device = GetDeviceNumber(deviceNumber);
            device.SetBit(port, bit, value);
        }

        /// <summary>
        ///     Beendet alle vorgänge und Verbindet alle Devices erneut
        /// </summary>
        public void Reconnect() {
            RemoveAllDevices();
            InitFactory();
        }

        /// <summary>
        ///     Beendet und entfernt alle Device im Manager
        /// </summary>
        public void RemoveAllDevices() {
            if (_deviceThreadManager != null) {
                _deviceThreadManager.StopAllDeviceThreads();
                _deviceThreadManager = null;
            }
            if (Devices == null) return;
            try {
                foreach (var deviceEntry in Devices) {
                    var isClosed = CloseDeviceFromFactory(deviceEntry.Key);
                    if (isClosed) RemoveDevice(deviceEntry.Key);
                }
            } catch (InvalidOperationException) {
                // kann ignoriert werden da alle Devices schon weg sind!
            }

            //   Devices.Clear();
            Devices = null;
        }

        /// <summary>
        ///     Holt ein Device aus dem Manager
        /// </summary>
        /// <param name="deviceNumber"></param>
        /// <returns>Null wenn kein Device gefunden wurde</returns>
        /// <exception cref="ArgumentException">wenn die uebergebene Nummer nicht Valiede ist</exception>
        public Device GetDeviceNumber(int deviceNumber) {
            try {
                LibaryUtils.CheckDeviceNumber(deviceNumber);
            } catch (ArgumentException e) {
                AddError(e.Message);
            }

            if (Devices != null)
                foreach (var deviceEntry in Devices) {
                    var device = deviceEntry.Value;
                    if (device.DeviceNumber == deviceNumber) return device;
                }
            AddCritical("Es wurde kein Device mit der Nummer: " + deviceNumber + " gefunden");
            return null;
        }

        /// <summary>
        ///     Holt ein Device aus dem Manager
        /// </summary>
        /// <param name="deviceNumber"></param>
        /// <returns>Null wenn kein Device gefunden wurde</returns>
        /// <exception cref="ArgumentException">wenn die uebergebene Nummer nicht Valiede ist</exception>
        public Device GetDeviceNumber(object deviceNumber) {
            try {
                var selectedDevice = Convert.ToInt32(deviceNumber);
                return GetDeviceNumber(selectedDevice);
            } catch (Exception) {
                AddCritical("Es wurde keine gültige Device Auswahl getroffen.");
            }
            return null;
        }

        /// <summary>
        ///     Get a Device by the Handler
        /// </summary>
        /// <param name="handler">handler of the Device we want</param>
        /// <returns>Device when found als null</returns>
        public Device GetDeviceFromHandler(int handler) {
            Device device;

            Devices.TryGetValue(handler, out device);

            if (device == null) AddError("Device mit dem Handler: " + handler + " konnte nicht gefunden werden.");
            return device;
        }

        /// <summary>
        ///     Number of the Devices handel in Factory
        /// </summary>
        /// <returns>0 to Number of Devices</returns>
        public int GetNumberOfDevices() {
            return Devices?.Count ?? 0;
        }

        private bool OpenConnectedDevices() {
            var firstDeviceHandler = IowKitFacade.OpenDevices();

            if (firstDeviceHandler != null) return true;

            AddError("Fehler beim öffnen der Geräte. Sind welche angeschlossen worden?");
            return false;
        }

        private bool CountConnectedDevices() {
            _deviceCounter = IowKitFacade.GetConnectDeviceCounter();

            if (_deviceCounter != null) return true;

            AddError("Fehler beim öffnen der Geräte. Sind welche angeschlossen worden?");
            return false;
        }

        private bool AddAllConnectedDeviceToFactory() {
            for (int? i = Defines.StartNumbering; i <= _deviceCounter; i++) {
                var handler = IowKitFacade.GetHandlerForDevice(i);

                if (handler == null) {
                    AddError("Das Device mit der Nummer: " + i + " konnte nicht geöffnet " +
                             "werden. Es wird nicht geladen.");
                    continue;
                }

                AddDeviceToFactory((int) handler, (int) i);
            }
            AddEvent("Alle Verbundenen Geräte sind erfolgreich gestartet worden.");
            return true;
        }

        private void AddDeviceToFactory(int handler, int deviceNumber) {
            if (Devices == null) Devices = new Dictionary<int, Device>();

            var device = new Device(handler) {DeviceNumber = deviceNumber};
            device.DeviceClose += Device_DeviceClose;
            device.InfoLog += InfoLog;
            device.EventLog += EventLog;
            device.ErrorLog += ErrorLog;
            device.CriticalLog += CriticalLog;

            // default is the IO Mode
            device.Mode = new IoMode();
            Devices.Add(handler, device);
        }

        private bool CloseDeviceFromFactory(int? handler) {
            Device device;

            if (handler == null) return false;

            Devices.TryGetValue((int) handler, out device);

            if (device != null) {
                device.Close();
                return true;
            }
            AddCritical("Device mit dem Handler: " + handler + " konnte nicht enfernt werden.");
            return false;
        }

        private void RemoveDevice(int? handler) {
            if ((handler != null) && (Devices != null))
                if (Devices.ContainsKey((int) handler)) {
                    AddEvent("Device mit dem Handler: " + handler + " wird enfernt");
                    Device device;
                    Devices.TryGetValue((int) handler, out device);
                    StopDevice(device);
                    Devices.Remove((int) handler);
                }
        }

        private void Device_DeviceClose(Device device) {
            if (device?.Handler != null) RemoveDeviceAsCloseEvent(device);
        }

        private void InfoLog(Device device) {
            Log.AddList(device.Log.GetLogEntryssInfoAndReset());
            DeviceInfo?.Invoke(this);
        }

        private void EventLog(Device device) {
            Log.AddList(device.Log.GetLogEntrysEventAndReset());
            DeviceEvent?.Invoke(this);
        }

        private void ErrorLog(Device device) {
            Log.AddList(device.Log.GetLogEntrysErrorAndReset());
            DeviceError?.Invoke(this);
        }

        private void CriticalLog(Device device) {
            Log.AddList(device.Log.GetAllLogEntries());

            StopDevice(device.DeviceNumber);
            RemoveDevice(device.Handler);
            device.Close();

            AddCritical("Aufgrund eines Kritischen Fehlers wurden alle Devices Beendet und entfernt. " +
                        device.Handler);
        }

        private void RemoveDeviceAsCloseEvent(Device device) {
            if (Devices == null) return;
            if (device.Handler == null) return;

            Device deviceFromFactroy;

            Devices.TryGetValue((int) device.Handler, out deviceFromFactroy);

            if (deviceFromFactroy != null) RemoveDevice(device.Handler);
        }

        private void AddEvent(string msg) {
            Log.AddEventLog(null, msg);
            DeviceEvent?.Invoke(this);
        }

        private void AddError(string msg) {
            Log.AddErrorLog(null, msg);
            DeviceError?.Invoke(this);
        }

        private void AddCritical(string msg) {
            Log.AddCriticalLog(null, msg);
            if (DeviceCritical == null) throw new SystemException("Critical Error at Devices handling: " + msg);
            DeviceCritical(this);
        }

        private void DeviceHandler_RunTimeUpdate(long runtime) {
            RunTimeUpdate?.Invoke(runtime);
        }
    }
}