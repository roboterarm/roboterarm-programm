﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Linq;
using IowLibrary.Devices.Model;

namespace IowLibrary.Devices.Control {
    /// <summary>
    ///     Logs die auftretten koennen diese Klasse ist universell in Devices und Manager zu finden.
    /// </summary>
    public class Log {
        private readonly List<LogEntry> _logs = new List<LogEntry>();

        /// <summary>
        ///     Add new LogEntry for a Device
        /// </summary>
        /// <param name="device">device</param>
        /// <param name="msg">log entry</param>
        /// <param name="level">log level</param>
        /// <returns>instance of this</returns>
        public Log AddLog(Device device, string msg, LogEntry.LogLevel level) {
            _logs.Add(LogEntry.NewLogEntry(device, msg, level));
            return this;
        }

        /// <summary>
        ///     Adds new LogEntry at Level Info
        /// </summary>
        /// <param name="device">device</param>
        /// <param name="msg">log entry</param>
        /// <returns>instance of this</returns>
        public Log AddInfoLog(Device device, string msg) {
            _logs.Add(LogEntry.NewLogEntry(device, msg, LogEntry.LogLevel.Info));
            return this;
        }

        /// <summary>
        ///     Adds new LogEntry at Level Info
        /// </summary>
        /// <param name="device">device</param>
        /// <param name="msg">log entry</param>
        /// <returns>instance of this</returns>
        public Log AddEventLog(Device device, string msg) {
            _logs.Add(LogEntry.NewLogEntry(device, msg, LogEntry.LogLevel.Event));
            return this;
        }


        /// <summary>
        ///     Adds new LogEntry at Level Error
        /// </summary>
        /// <param name="device">device</param>
        /// <param name="msg">log entry</param>
        /// <returns>instance of this</returns>
        public Log AddErrorLog(Device device, string msg) {
            _logs.Add(LogEntry.NewLogEntry(device, msg, LogEntry.LogLevel.Error));
            return this;
        }

        /// <summary>
        ///     Adds new LogEntry at Level Critical
        /// </summary>
        /// <param name="device">device</param>
        /// <param name="msg">log entry</param>
        /// <returns>instance of this</returns>
        public Log AddCriticalLog(Device device, string msg) {
            _logs.Add(LogEntry.NewLogEntry(device, msg, LogEntry.LogLevel.Critical));
            return this;
        }

        /// <summary>
        ///     Retuns all Log entries holds in object
        /// </summary>
        /// <returns>List of Log Entries</returns>
        public List<LogEntry> GetAllLogEntries() {
            return _logs;
        }

        /// <summary>
        ///     Returns logs entry at the specified level
        /// </summary>
        /// <param name="level">Log Level</param>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrieyAtLevel(LogEntry.LogLevel level) {
            var logEntries = new List<LogEntry>();
            // Keine Logs abbrechen
            if (_logs.Count == 0) {
                return logEntries;
            }
            foreach (var logEntry in _logs.ToList())
                if ((logEntry != null) && logEntry.Level.Equals(level)) logEntries.Add(logEntry);
            return logEntries;
        }

        /// <summary>
        ///     Returns events log entries at Level Info
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysInfo() {
            return GetLogEntrieyAtLevel(LogEntry.LogLevel.Info);
        }

        /// <summary>
        ///     Returns events log entries and reset cache
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntryssInfoAndReset() {
            var list = GetLogEntrysInfo();
            ClearLog();
            return list;
        }

        /// <summary>
        ///     Returns events log entries at Level Event
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysEvent() {
            return GetLogEntrieyAtLevel(LogEntry.LogLevel.Event);
        }

        /// <summary>
        ///     Returns events log entries and reset cache
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysEventAndReset() {
            var list = GetLogEntrysEvent();
            ClearLog();
            return list;
        }

        /// <summary>
        ///     Returns error log entries
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysError() {
            return GetLogEntrieyAtLevel(LogEntry.LogLevel.Error);
        }

        /// <summary>
        ///     Returns error log entries and reset Log Cache
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysErrorAndReset() {
            var logs = GetLogEntrysError();
            ClearLog();
            return logs;
        }

        /// <summary>
        ///     Returns all None Critical LogEntrys
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogAllNoneCrtical() {
            var list = GetLogEntrysEvent();
            list.AddRange(GetLogEntrysInfo());
            list.AddRange(GetLogEntrysError());
            ClearLog();
            return list;
        }

        /// <summary>
        ///     Returns Critical log entries
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysCritcal() {
            return GetLogEntrieyAtLevel(LogEntry.LogLevel.Critical);
        }

        /// <summary>
        ///     Returns Critical log entries and reset Log Cache
        /// </summary>
        /// <returns>List of log entry at Level</returns>
        public List<LogEntry> GetLogEntrysCriticalAndReset() {
            var logs = GetLogEntrysCritcal();
            ClearLog();
            return logs;
        }

        /// <summary>
        ///     Clears the Log
        /// </summary>
        public void ClearLog() {
            _logs.Clear();
        }

        /// <summary>
        ///     Returns a new Instacne of this Class
        /// </summary>
        /// <returns>Log instance</returns>
        public static Log NewInstance() {
            return new Log();
        }

        /// <summary>
        ///     Puts an List of Logs to this Instance an sort them
        /// </summary>
        /// <param name="logList">list of logs to add</param>
        public void AddList(List<LogEntry> logList) {
            _logs.AddRange(logList);
        }
    }
}