﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

namespace Roboter.Propertys {
    public class Axis2Property : IAxisProperty {
        private readonly Propertys _propertys;

        public Axis2Property(Propertys propertys) {
            _propertys = propertys;
        }

        public int PulsMin() {
            return _propertys.Axis2PulsLeft;
        }

        public int PulsMax() {
            return _propertys.Axis2PulsRight;
        }

        public void SetPulsMin(int min) {
            _propertys.Axis2PulsLeft = min;
        }

        public void SetPulsMax(int max) {
            _propertys.Axis2PulsRight = max;
        }

        public string Name() {
            return _propertys.Axis2Name;
        }

        public bool Invert() {
            return _propertys.Axis2Invert;
        }

        public Propertys Propertys() {
            return _propertys;
        }

        public int MaxUser() {
            return _propertys.Axis2UserMax;
        }

        public int MinUser() {
            return _propertys.Axis2UserMin;
        }

        public int Speed() {
            return _propertys.Axis2Speed;
        }
    }
}