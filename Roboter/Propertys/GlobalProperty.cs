﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

namespace Roboter.Propertys {
    /// <summary>
    ///     Enhält alle Instanzen der Propertys
    /// </summary>
    public class GlobalProperty {
        private static GlobalProperty _instance;

        private GlobalProperty(Propertys propertys) {
            Propertys = propertys;
            Axis1 = new Axis1Property(propertys);
            Axis2 = new Axis2Property(propertys);
            Axis3 = new Axis3Property(propertys);
            Axis4 = new Axis4Property(propertys);
        }

        /// <summary>
        ///     Achse 1
        /// </summary>
        public Axis1Property Axis1 { get; set; }

        /// <summary>
        ///     Achse 2
        /// </summary>
        public Axis2Property Axis2 { get; set; }

        /// <summary>
        ///     Achse 3
        /// </summary>
        public Axis3Property Axis3 { get; set; }

        /// <summary>
        ///     Achse 4
        /// </summary>
        public Axis4Property Axis4 { get; set; }

        /// <summary>
        ///     Rückgabe der Allgemeinen Properyts
        /// </summary>
        public Propertys Propertys { get; }

        /// <summary>
        ///     Rückgabe der Statischen Instanz der Propertys
        /// </summary>
        /// <returns></returns>
        public static GlobalProperty GetInstance() {
            if (_instance == null) _instance = new GlobalProperty(new Propertys());

            return _instance;
        }
    }
}