﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.ComponentModel;
using System.Configuration;

namespace Roboter.Propertys {
    public class Propertys : ApplicationSettingsBase {
        [Category("Allgemein")]
        [DisplayName("Zykluszeit")]
        [Description("Zykluszeit des PMW Signals in ms")]
        [UserScopedSetting]
        [DefaultSettingValue("20")]
        public int DutyTime {
            get { return (int) this["DutyTime"]; }
            set { this["DutyTime"] = value; }
        }

        [Category("Allgemein")]
        [DisplayName("Achsen Geschwindigkeit Minimal")]
        [Description("Minimale Achsengeschwindigkeit die gewählt werden kann")]
        [UserScopedSetting]
        [DefaultSettingValue("1")]
        public int AxisSpeedMin {
            get { return (int) this["AxisSpeedMin"]; }
            set { this["AxisSpeedMin"] = value; }
        }

        [Category("Allgemein")]
        [DisplayName("Achsen Geschwindigkeit Maximal")]
        [Description("Maximale Achsengeschwindigkeit die gewählt werden kann")]
        [UserScopedSetting]
        [DefaultSettingValue("100")]
        public int AxisSpeedMax {
            get { return (int) this["AxisSpeedMax"]; }
            set { this["AxisSpeedMax"] = value; }
        }

        [Category("Allgemein")]
        [DisplayName("PCA9685 I2C Adresse")]
        [Description("Adresse der PMW-Moduls auf dem I2C Bus")]
        [UserScopedSetting]
        [DefaultSettingValue("192")]
        public int Pmwi2CAdresse {
            get { return (int) this["Pmwi2CAdresse"]; }
            set { this["Pmwi2CAdresse"] = value; }
        }

        /// <summary>
        ///     Achse 1
        /// </summary>
        [Category("Achse 1")]
        [DisplayName("Pulsweite Links")]
        [Description("Pulsweite für den Linksanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("250")]
        public int Axis1PulsLeft {
            get { return (int) this["Axis1PulsLeft"]; }
            set { this["Axis1PulsLeft"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Pulsweite Rechts")]
        [Description("Pulsweite für den Rechtsanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("2000")]
        public int Axis1PulsRight {
            get { return (int) this["Axis1PulsRight"]; }
            set { this["Axis1PulsRight"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Invertieren")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("false")]
        public bool Axis1Invert {
            get { return (bool) this["Axis1Invert"]; }
            set { this["Axis1Invert"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Name")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("Achse 1 Drehen")]
        public string Axis1Name {
            get { return (string) this["Axis1Name"]; }
            set { this["Axis1Name"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Schwenkwert Minimal")]
        [Description("Minimaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("0")]
        public int Axis1UserMin {
            get { return (int) this["Axis1UserMin"]; }
            set { this["Axis1UserMin"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Schwenkwert Maximal")]
        [Description("Maximaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("360")]
        public int Axis1UserMax {
            get { return (int) this["Axis1UserMax"]; }
            set { this["Axis1UserMax"] = value; }
        }

        [Category("Achse 1")]
        [DisplayName("Stellzeit der Achse")]
        [Description("Dauer die der Servo braucht um den ganzen Fahrbereich zu  fahren in ms")]
        [UserScopedSetting]
        [DefaultSettingValue("1000")]
        public int Axis1Speed {
            get { return (int) this["Axis1Speed"]; }
            set { this["Axis1Speed"] = value; }
        }

        /// <summary>
        ///     Achse 2
        /// </summary>
        [Category("Achse 2")]
        [DisplayName("Pulsweite Links")]
        [Description("Pulsweite für den Linksanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("1000")]
        public int Axis2PulsLeft {
            get { return (int) this["Axis2PulsLeft"]; }
            set { this["Axis2PulsLeft"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Pulsweite Rechts")]
        [Description("Pulsweite für den Rechtsanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("2000")]
        public int Axis2PulsRight {
            get { return (int) this["Axis2PulsRight"]; }
            set { this["Axis2PulsRight"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Invertieren")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("false")]
        public bool Axis2Invert {
            get { return (bool) this["Axis2Invert"]; }
            set { this["Axis2Invert"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Name")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("Achse 2 Arm vorne")]
        public string Axis2Name {
            get { return (string) this["Axis2Name"]; }
            set { this["Axis2Name"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Schwenkwert Minimal")]
        [Description("Minimaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("0")]
        public int Axis2UserMin {
            get { return (int) this["Axis2UserMin"]; }
            set { this["Axis2UserMin"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Schwenkwert Maximal")]
        [Description("Maximaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("360")]
        public int Axis2UserMax {
            get { return (int) this["Axis2UserMax"]; }
            set { this["Axis2UserMax"] = value; }
        }

        [Category("Achse 2")]
        [DisplayName("Stellzeit der Achse")]
        [Description("Dauer die der Servo braucht um den ganzen Fahrbereich zu  fahren in ms")]
        [UserScopedSetting]
        [DefaultSettingValue("1000")]
        public int Axis2Speed {
            get { return (int) this["Axis2Speed"]; }
            set { this["Axis2Speed"] = value; }
        }

        /// <summary>
        ///     Achse 3
        /// </summary>
        [Category("Achse 3")]
        [DisplayName("Pulsweite Links")]
        [Description("Pulsweite für den Linksanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("750")]
        public int Axis3PulsLeft {
            get { return (int) this["Axis3PulsLeft"]; }
            set { this["Axis3PulsLeft"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Pulsweite Rechts")]
        [Description("Pulsweite für den Rechtsanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("1600")]
        public int Axis3PulsRight {
            get { return (int) this["Axis3PulsRight"]; }
            set { this["Axis3PulsRight"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Invertieren")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("false")]
        public bool Axis3Invert {
            get { return (bool) this["Axis3Invert"]; }
            set { this["Axis3Invert"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Name")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("Achse 3 Arm hinten")]
        public string Axis3Name {
            get { return (string) this["Axis3Name"]; }
            set { this["Axis3Name"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Schwenkwert Minimal")]
        [Description("Minimaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("0")]
        public int Axis3UserMin {
            get { return (int) this["Axis3UserMin"]; }
            set { this["Axis3UserMin"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Schwenkwert Maximal")]
        [Description("Maximaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("360")]
        public int Axis3UserMax {
            get { return (int) this["Axis3UserMax"]; }
            set { this["Axis3UserMax"] = value; }
        }

        [Category("Achse 3")]
        [DisplayName("Stellzeit der Achse")]
        [Description("Dauer die der Servo braucht um den ganzen Fahrbereich zu  fahren in ms")]
        [UserScopedSetting]
        [DefaultSettingValue("1000")]
        public int Axis3Speed {
            get { return (int) this["Axis3Speed"]; }
            set { this["Axis3Speed"] = value; }
        }

        /// <summary>
        ///     Achse 4
        /// </summary>
        [Category("Achse 4")]
        [DisplayName("Pulsweite Links")]
        [Description("Pulsweite für den Linksanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("900")]
        public int Axis4PulsLeft {
            get { return (int) this["Axis4PulsLeft"]; }
            set { this["Axis4PulsLeft"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Pulsweite Rechts")]
        [Description("Pulsweite für den Rechtsanschlag in µs")]
        [UserScopedSetting]
        [DefaultSettingValue("2450")]
        public int Axis4PulsRight {
            get { return (int) this["Axis4PulsRight"]; }
            set { this["Axis4PulsRight"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Invertieren")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("false")]
        public bool Axis4Invert {
            get { return (bool) this["Axis4Invert"]; }
            set { this["Axis4Invert"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Name")]
        [Description("Invertiert die Drehrichtung der Achse")]
        [UserScopedSetting]
        [DefaultSettingValue("Achse 4 Greifer")]
        public string Axis4Name {
            get { return (string) this["Axis4Name"]; }
            set { this["Axis4Name"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Schwenkwert Minimal")]
        [Description("Minimaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("0")]
        public int Axis4UserMin {
            get { return (int) this["Axis4UserMin"]; }
            set { this["Axis4UserMin"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Schwenkwert Maximal")]
        [Description("Maximaler Schwenkwert den der User angeben kann")]
        [UserScopedSetting]
        [DefaultSettingValue("360")]
        public int Axis4UserMax {
            get { return (int) this["Axis4UserMax"]; }
            set { this["Axis4UserMax"] = value; }
        }

        [Category("Achse 4")]
        [DisplayName("Stellzeit der Achse")]
        [Description("Dauer die der Servo braucht um den ganzen Fahrbereich zu  fahren in ms")]
        [UserScopedSetting]
        [DefaultSettingValue("1500")]
        public int Axis4Speed {
            get { return (int) this["Axis4Speed"]; }
            set { this["Axis4Speed"] = value; }
        }
    }
}