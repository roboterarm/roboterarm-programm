﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Diagnostics;
using System.Threading;
using Roboter.Control;

namespace Roboter.scriptcode {
    /// <summary>
    ///     Wartet eine diffinierte Zeit an einem bestimmten Punkt des Programms
    /// </summary>
    internal class Wait : Command, ICommand {
        private const int Max = 20000;
        private const int Min = 1;

        /// <summary>
        ///     Erzeugen eines neuen Move Commands
        /// </summary>
        /// <param name="axisNumber"></param>
        /// <param name="value"></param>
        public Wait(int? value) {
            if (value != null) Value = (int) value;
        }

        /// <summary>
        ///     Nummer der Achse die angesprochen werden soll
        /// </summary>
        public int AxisNumber { get; set; }

        /// <summary>
        ///     wert um den Die Achse verstellt werden soll
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        ///     Führt den ScriptCode aus
        /// </summary>
        /// <param name="axisController"></param>
        public override void Run(AxisController axisController) {
            Sleep(Value);
        }

        public bool Validate() {
            if ((Value < Max) || (Value > Min)) return false;
            return true;
        }

        public string ToScriptCode() {
            return "wait(" + Value + ")";
        }

        public override string ToString() {
            return ToScriptCode();
        }

        private void Sleep(long milliseconds) {
            if (milliseconds <= 0) return;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds < milliseconds) {
                var timeout = milliseconds - stopwatch.ElapsedMilliseconds;
                Thread.Sleep((int) (timeout >= 0 ? timeout : 0));
            }

            stopwatch.Stop();
        }
    }
}