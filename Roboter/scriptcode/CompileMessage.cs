﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

namespace Roboter.scriptcode {
    /// <summary>
    ///     Messages die wären des Compilierens anlaufen
    /// </summary>
    public class CompileMessage {
        private CompileMessage(int lineNumber, string message) {
            LineNumber = lineNumber;
            Message = message;
        }

        /// <summary>
        ///     Zeilennummer des Message
        /// </summary>
        public int LineNumber { get; set; }

        /// <summary>
        ///     Nachricht der Zeile
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        ///     Rückgabe als String
        /// </summary>
        /// <returns>Zeilennummer + Message</returns>
        public override string ToString() {
            return LineNumber + ": " + Message;
        }

        /// <summary>
        ///     Erzeugen einer neuen Instanz
        /// </summary>
        /// <param name="lineNumber">Zeilennummer</param>
        /// <param name="message">Nachricht</param>
        /// <returns>neue Instanz von CompileMessage</returns>
        public static CompileMessage Create(int lineNumber, string message) {
            return new CompileMessage(lineNumber, message);
        }
    }
}