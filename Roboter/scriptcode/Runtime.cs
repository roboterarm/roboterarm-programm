﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Roboter.Control;

namespace Roboter.scriptcode {
    public delegate void RuntimeProgress(string msg);

    /// <summary>
    ///     Runtime für den ScriptCode
    /// </summary>
    public class Runtime {
        private readonly AxisController _axisController;

        private IList<ICommand> _commands;
        private Thread _thread;
        private RunTimeThreading _threadObject;

        public Runtime(AxisController axisController) {
            _axisController = axisController;
        }

        /// <summary>
        ///     Commmands aus dem ScriptCode
        /// </summary>
        public IList<ICommand> Commands {
            set { _commands = value; }
        }

        public event RuntimeProgress Progress;
        public event RuntimeProgress Done;

        /// <summary>
        ///     Starten der Runtime
        /// </summary>
        public void Run() {
            _threadObject = new RunTimeThreading();
            _threadObject.Commands = _commands;
            _threadObject.AxisController = _axisController;
            _threadObject.Progress += ProgressUpdate;
            _threadObject.Done += RuntimeDone;

            _thread = new Thread(_threadObject.Run);
            _thread.Start();
        }

        /// <summary>
        ///     Abbrechen der Runtime
        /// </summary>
        public void Cancel() {
            _threadObject.Cancel();
        }

        protected void ProgressUpdate(string msg) {
            Progress?.Invoke(msg);
        }

        protected void RuntimeDone(string msg) {
            Done?.Invoke(msg);
        }


        /// <summary>
        ///     Wörker Object für die Runtimer
        /// </summary>
        private class RunTimeThreading {
            private AxisController _axisController;
            private IList<ICommand> _commands;
            private bool _stop;

            /// <summary>
            ///     Commmands aus dem ScriptCode
            /// </summary>
            public IList<ICommand> Commands {
                set { _commands = value; }
            }

            /// <summary>
            ///     Commmands aus dem ScriptCode
            /// </summary>
            public AxisController AxisController {
                set { _axisController = value; }
            }

            public event RuntimeProgress Progress;
            public event RuntimeProgress Done;

            /// <summary>
            ///     Arbeits Methode, Thread Endet mit ende des ScriptCodes
            /// </summary>
            public void Run() {
                try {
                    _stop = false;
                    foreach (var command in _commands.ToList()) {
                        if (_stop) break;
                        Progress?.Invoke(command.ToString());
                        command.Run(_axisController);
                    }
                    Done?.Invoke("");
                } catch (ThreadInterruptedException) {
                    var achseCount = _axisController.GetAllRegistertAxis().Count;
                    for (var i = achseCount; i >= 1; i--) {
                        _axisController.ChangeActive(i, true);
                        // iow gedenkzeit
                        Thread.Sleep(50);
                    }
                }
            }

            /// <summary>
            ///     Abbrechen der Arbeits for Schleife
            /// </summary>
            public void Cancel() {
                _stop = true;
            }
        }
    }
}