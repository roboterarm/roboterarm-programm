﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;

namespace Roboter.scriptcode {
    /// <summary>
    ///     Compiler Events
    /// </summary>
    public delegate void CompilerEvent();

    /// <summary>
    ///     Compiliert den eingebenen Script Code
    /// </summary>
    public class Compiler {
        /// <summary>
        ///     Default Values des ScriptCodes
        /// </summary>
        private const string Open = "(";

        private const string Close = ")";
        private const string ValueSeparator = ",";
        private const string ValueTrue = "true";
        private const string ValueFalse = "false";
        private const string Comment = "//";

        /// <summary>
        ///     Move Commando Simple
        /// </summary>
        private const string MoveCommand = "move";

        private const int MoveCommandValueNumber = 2;

        /// <summary>
        ///     Enable Commando Simple
        /// </summary>
        private const string EnableCommand = "enable";

        private const int EnableCommandValueNumber = 2;

        /// <summary>
        ///     Enable Commando Simple
        /// </summary>
        private const string WaitCommand = "wait";

        private const int WaitCommandValueNumber = 1;

        /// <summary>
        ///     List der aus dem ScriptCode ermittelten Commands
        /// </summary>
        public IList<ICommand> Commands { get; } = new List<ICommand>();

        /// <summary>
        ///     Rückgabe der wären des Compilierens aufgetrennen Meldungen
        /// </summary>
        public IList<CompileMessage> CompileMessages { get; } = new List<CompileMessage>();

        public event CompilerEvent EventCompilerDone;

        /// <summary>
        ///     Compiliert den eigeben ScriptCode
        /// </summary>
        /// <param name="scriptCode">Code der Compiliert werden soll</param>
        public void Compile(string scriptCode) {
            CompileMessages.Clear();
            Commands.Clear();

            var lines = SeperateLines(scriptCode);

            var lineNumber = 0;
            foreach (var line in lines) {
                lineNumber++;
                // leerezeilen ignorieren
                if (line.TrimStart().Equals("")) continue;

                // commentare ignore
                if (line.StartsWith(Comment)) continue;

                // OpenBrace Index
                int indexOfOpenBrace;
                if (GetOpenBraceIndex(line, lineNumber, out indexOfOpenBrace)) return;

                // CloseBrace Index
                int indexOfCloseBrace;
                if (GetCloseBraceIndex(line, lineNumber, out indexOfCloseBrace)) return;

                var command = line.Substring(0, indexOfOpenBrace);
                var value = line.Substring(indexOfOpenBrace + 1, indexOfCloseBrace - indexOfOpenBrace - 1);

                if (command.Equals("")) {
                    AddCompileMessage(lineNumber, "kein Kommando");
                    continue;
                }

                command = command.ToLower();
                value = value.ToLower();
                switch (command) {
                    case MoveCommand:
                        ParseMoveCommand(lineNumber, value);
                        break;
                    case EnableCommand:
                        ParseEnableCommand(lineNumber, value);
                        break;
                    case WaitCommand:
                    ParseWaitCommand(lineNumber, value);
                    break;
                    default:
                        AddCompileMessage(lineNumber, "kein gültiges Kommando erkannt");
                        break;
                }
            }
            CompilerDone();
        }

        /// <summary>
        ///     Splitet die Linien des ScriptCodes auf.
        /// </summary>
        /// <param name="scriptCode">Script Code als String</param>
        /// <returns>Die einzelnen Linien</returns>
        private static IEnumerable<string> SeperateLines(string scriptCode) {
            return scriptCode.Split(new[] {Environment.NewLine}, StringSplitOptions.None);
        }

        /// <summary>
        ///     Gibt den Index eines sie öffnenden Klammer zurück. Wenn es keine gibt wird eine CompilerMsg hinzugefügt
        /// </summary>
        /// <param name="line">Linie in der das Kommando steht</param>
        /// <param name="lineNumber">Linien Nummer</param>
        /// <param name="indexOfOpenBrace"></param>
        /// <returns>True wenn es eine gibt Sonst ein False</returns>
        private bool GetOpenBraceIndex(string line, int lineNumber, out int indexOfOpenBrace) {
            indexOfOpenBrace = line.IndexOf(Open, StringComparison.Ordinal);
            if (indexOfOpenBrace != -1) return false;
            AddCompileMessage(lineNumber, "Klammerfehler: \"(\" ");
            return true;
        }

        /// <summary>
        ///     Gibt den index der Schließenden Klamme zurück. Ist keine vorhanden wird eine CompilerMessage hinzugefügt.
        /// </summary>
        /// <param name="line">Linie in der das Kommando steht</param>
        /// <param name="lineNumber">Linien Nummer</param>
        /// <param name="indexOfCloseBrace"></param>
        /// <returns>True wenn es eine Gibt wenn nicht ein False</returns>
        private bool GetCloseBraceIndex(string line, int lineNumber, out int indexOfCloseBrace) {
            indexOfCloseBrace = line.IndexOf(Close, StringComparison.Ordinal);
            if (indexOfCloseBrace != -1) return false;
            AddCompileMessage(lineNumber, "Klammerfehler: \")\" ");
            return true;
        }

        /// <summary>
        ///     Parsen eines Enable Commandos
        /// </summary>
        /// <param name="lineNumber">Linien Nummer</param>
        /// <param name="line">commando Line</param>
        private void ParseEnableCommand(int lineNumber, string line) {
            var values = line.Split(new[] {ValueSeparator}, StringSplitOptions.None);
            if (values.Length != EnableCommandValueNumber)
                AddCompileMessage(lineNumber, "Ungültige Anzahl der Werten, erwartet: " + EnableCommandValueNumber);
            var axisnumber = ParseInt(lineNumber, values[0]);
            var status = ParseBool(lineNumber, values[1]);
            if ((axisnumber == null) || (status == null)) return;
            Commands.Add(new Enable(axisnumber, status));
        }

        /// <summary>
        ///     Parsen eines Move Commands
        /// </summary>
        /// <param name="lineNumber">Linien Nummer</param>
        /// <param name="line">commando Linie</param>
        private void ParseMoveCommand(int lineNumber, string line) {
            var values = line.Split(new[] {ValueSeparator}, StringSplitOptions.None);
            if (values.Length != MoveCommandValueNumber)
                AddCompileMessage(lineNumber, "Ungültige Anzahl der Werten, erwartet: " + MoveCommandValueNumber);
            var axisnumber = ParseInt(lineNumber, values[0]);
            var axisMoveValue = ParseInt(lineNumber, values[1]);
            if ((axisnumber == null) || (axisMoveValue == null)) return;
            Commands.Add(new Move(axisnumber, axisMoveValue));
        }

        private void ParseWaitCommand(int lineNumber, string line) {
            var values = line.Split(new[] { ValueSeparator }, StringSplitOptions.None);
            if (values.Length != WaitCommandValueNumber)
                AddCompileMessage(lineNumber, "Ungültige Anzahl der Werten, erwartet: " + WaitCommandValueNumber);
            var waitTime = ParseInt(lineNumber, values[0]);
            if ((waitTime == null)) return;
            Commands.Add(new Wait(waitTime));
        }

        /// <summary>
        ///     Parsen eines interger wertes aus einem String
        /// </summary>
        /// <param name="lineNumber">line in der das Command steht</param>
        /// <param name="value">string abbildungen des Int Werts</param>
        /// <returns>int wenn gültig</returns>
        private int? ParseInt(int lineNumber, string value) {
            value = value.Trim();
            try {
                return Convert.ToInt32(value);
            } catch (Exception) {
                AddCompileMessage(lineNumber, "Kein gültiger Zahlenwert: " + value);
                return null;
            }
        }

        /// <summary>
        ///     Parsen von Boolschen werten. Zulässig ist true
        ///     oder false als string, leerzeichen vorne und hinten werden entfernt.
        /// </summary>
        /// <param name="lineNumber">line in der das Command steht</param>
        /// <param name="value">String wert true oder false</param>
        /// <returns>bool wenn gültig</returns>
        private bool? ParseBool(int lineNumber, string value) {
            value = value.Trim();
            switch (value) {
                case ValueTrue:
                    return true;
                case ValueFalse:
                    return false;
                default:
                    AddCompileMessage(lineNumber, "Kein gültiger Boolscher wert: " + value);
                    return null;
            }
        }

        private void AddCompileMessage(int lineNumber, string msg) {
            CompileMessages.Add(CompileMessage.Create(lineNumber, msg));
        }

        private void CompilerDone() {
            if (EventCompilerDone != null) EventCompilerDone();
        }
    }
}