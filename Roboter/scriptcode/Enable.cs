﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Threading;
using Roboter.Control;

namespace Roboter.scriptcode {
    public class Enable : Command {
        private bool _isActiv;

        public Enable(int? axisNumber, bool? isActive) {
            if (axisNumber != null) AxisNumber = (int) axisNumber;
            if (isActive != null) IsActiv = (bool) isActive;
        }

        public bool IsActiv {
            get { return _isActiv; }
            set {
                // Drehen das Aktiv Status für PNP Transistoren am Ausgang
                _isActiv = !value;
            }
        }

        public int AxisNumber { get; set; }


        public override void Run(AxisController axisController) {
            if (axisController == null) return;

            axisController.ChangeActive(AxisNumber, IsActiv);
            // der kackwarrior braucht zeit...
            Thread.Sleep(10);
        }

        public string ToScriptCode() {
            return "enable(" + AxisNumber + "," + IsActiv + ")";
        }

        public override string ToString() {
            return ToScriptCode();
        }

        public override bool Validate() {
            return true;
        }
    }
}