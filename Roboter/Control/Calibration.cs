﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Diagnostics;
using System.Threading;
using IowLibrary.Devices.Model;

namespace Roboter.Control {
    public class Calibration {
        private const int TragetDifferenz = 5;
        private const int MaxSearchPoint = 25;

        private readonly AxisController _axisController;
        private int _max = 360;
        private int _min;

        // Volatile is used as hint to the compiler that this data
        // member will be accessed by multiple threads.
        private volatile bool _shouldStop;
        public CalibrationDoneCallback CalibrationDoneCallback;

        public Calibration(AxisController axisController) {
            _axisController = axisController;
            _axisController.OverLoadPositionEvent += _axisController_OverLoadPositionEvent;
        }

        private void _axisController_OverLoadPositionEvent(int axisNumber, bool state) {
        }

        // This method will be called when the thread is started.
        public void DoWork() {
            var achseCount = _axisController.GetAllRegistertAxis().Count;
            try {
                for (var i = achseCount; i >= 1; i--) {
                    _axisController.ChangeActive(i, true);
                    // iow gedenkzeit
                    SleepExact(50);
                }

                for (var i = achseCount; i >= 1; i--) {
                    _axisController.ChangeActive(i, false);
                    var axis = _axisController.GetAxis(i);
                    _min = axis.MinUser;
                    _max = axis.MaxUser;

                    // Alte werte abrufen vor dem Speichern
                    var oldMaxPulse = axis.MaxPulse;
                    var oldMinPuls = axis.MinPulse;

                    var newMin = SearchMinPoint(i);
                    var newMax = SearchMaxPoint(i);
                    // Wichtig! Achsen sollen nacher immer auf 0 gefahren werden...
                    var sleep = _axisController.MoveAxis(i, 0);
                    SleepExact(sleep);
                    _axisController.ChangeActive(i, true);

                    // iow gedenkzeit
                    SleepExact(50);

                    _axisController.Device.AddEventLog("Neuer Min wert: " + newMin);
                    _axisController.Device.AddEventLog("Neuer Max wert: " + newMax);
                    // Set AxisValue to Axis
                    _axisController.ChangeAxisMinValue(i, newMin, oldMaxPulse);
                    _axisController.ChangeAxisMaxValue(i, newMax, oldMinPuls);
                }


                CalibrationDoneCallback?.Invoke();
            } catch (ThreadInterruptedException) {
                // rollback
                for (var i = achseCount; i >= 1; i--) {
                    _axisController.ChangeActive(i, true);
                    // iow gedenkzeit
                    SleepExact(50);
                }
                _axisController.Device.AddEventLog("Kalibierung Abgebrochen");
            }
        }

        private int SearchMinPoint(int axisNumber) {
            var sleep = _axisController.MoveAxis(axisNumber, _max/2);
            SleepExact(sleep);

            var calibrationData = new CalibrationData {
                MinOrMax = true,
                LastMaxPoint = _max/2,
                LastMinPoint = _min,
                LastResultPosition = _max / 2
            };
            return SearchPoint(axisNumber, calibrationData);
        }

        private int SearchMaxPoint(int axisNumber) {
            var sleep = _axisController.MoveAxis(axisNumber, _max/2);
            SleepExact(sleep);

            var calibrationData = new CalibrationData {
                MinOrMax = false,
                LastMaxPoint = _max,
                LastMinPoint = _max/2,
                LastResultPosition = _max
            };
            return SearchPoint(axisNumber, calibrationData);
        }

        /// <summary>
        ///     Sucht nach dem Minimalen oder Maximal Punkt
        /// </summary>
        /// <param name="axisNumber">Nummer der Achse</param>
        /// <param name="calibrationData"></param>
        /// <returns></returns>
        private int SearchPoint(int axisNumber, CalibrationData calibrationData) {
            if (_shouldStop) throw new ThreadInterruptedException();
            if (calibrationData.IsMaximumReached() || calibrationData.IsTargetReached())
                return calibrationData.LastResultPosition;

            var value = calibrationData.GetNextValue();

            // Punkt Anfahren
            _axisController.Device.AddEventLog("Achse " + axisNumber + " Wert " + value);
            var sleep = _axisController.MoveAxis(axisNumber, value);
            SleepExact(sleep);

            // 200 Ms Lang Messpunkte Sammeln
            var countTrueValue = 0;
            for (var i = 0; i < 100; i++) {
                var state1 = _axisController.GetAxisOverload(axisNumber);
                if ((state1 != null) && state1.Value) countTrueValue++;
                SleepExact(10);
            }
            // Mehr als 60% der werte waren true
            if (countTrueValue >= 80) {
                if (calibrationData.MinOrMax) {
                    calibrationData.LastMinPoint = calibrationData.LastResultPosition;
                } else {
                    calibrationData.LastMaxPoint = calibrationData.LastResultPosition;
                }
            } else {
                if (calibrationData.MinOrMax) {
                    calibrationData.LastMaxPoint = calibrationData.LastResultPosition;
                } else {
                    calibrationData.LastMinPoint = calibrationData.LastResultPosition;
                }

            }
            return SearchPoint(axisNumber, calibrationData);
        }

        /// <summary>
        ///     Stoppt die Kalibierung
        /// </summary>
        public void RequestStop() {
            _shouldStop = true;
        }


        public void SleepExact(long milliseconds) {
            if (milliseconds <= 0) return;

            var stopwatch = new Stopwatch();
            stopwatch.Start();

            while (stopwatch.ElapsedMilliseconds < milliseconds) {
                var timeout = milliseconds - stopwatch.ElapsedMilliseconds;
                Thread.Sleep((int) (timeout >= 0 ? timeout : 0));
            }

            stopwatch.Stop();
        }

        private class CalibrationData {
            /// <summary>
            ///     Schutz vor unendlichen Rekrusiven aufrufen
            /// </summary>
            private int _counterSearchPoint;

            public int LastMaxPoint;
            public int LastMinPoint;

            public int LastResultPosition;

            /// <summary>
            ///     True Richtung Min False Richtung Max
            /// </summary>
            public bool MinOrMax;

            public bool IsTargetReached() {
                if (MinOrMax) return LastMaxPoint - LastMinPoint <= TragetDifferenz;
                return LastMinPoint - LastMaxPoint >= -TragetDifferenz;
            }

            public bool IsMaximumReached() {
                return _counterSearchPoint >= MaxSearchPoint;
            }

            public int GetNextValue() {
                // ((Max - Min) / 2) + Min
                LastResultPosition = (LastMaxPoint - LastMinPoint)/2 + LastMinPoint;
                _counterSearchPoint++;
                return LastResultPosition;
            }
        }
    }
}