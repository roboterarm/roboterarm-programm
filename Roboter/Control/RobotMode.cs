﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.Linq;
using IowLibrary.Devices.Model;
using IowLibrary.IowKit;
using IowLibrary.IowKit.DllAdapter;
using IowLibrary.Modes;
using IowLibrary.Utils;

namespace Roboter.Control {
    /// <summary>
    ///     RoboterModus für die I2C Library
    /// </summary>
    public class RobotMode : IMode {
        /// <summary>
        ///     Anzahl der Versuche die gemacht wird bevor wir scheitern
        /// </summary>
        private const int Retrys = 3;

        private readonly IList<I2CData> _dataQueue = new List<I2CData>();

        private Device _device;
        private bool _readFirst;

        private int _readIoRetryCounter;
        private IList<I2CData> _removeList;
        private int _writei2CRetryCounter;
        private int _writeIoRetryCounter;

        /// <summary>
        ///     Setzt das Device in das
        /// </summary>
        /// <param name="device"></param>
        public void SetDevice(Device device) {
            _device = device;
        }

        /// <summary>
        ///     Starten des Modus und setzt den IOW in den I2C Special Mode
        /// </summary>
        /// <returns>true bei Erfolg sonst bricht der IOW ab.</returns>
        public bool PortsInitialisation() {
            var report = new byte[Defines.SpecialReportSize];

            // I2C Mode
            report[0] = 0X01;

            // enable
            report[1] = 0X01;

            // flags
            report[2] = 0X00;

            // timeout
            report[3] = 0X00;

            // Setzt das TimeOut damit wenn kein I2C Baustein da ist abgebrochen wird.
            IowKitFacade.Timeout(_device.Handler, 1000);

            // Setzten des I2C Mode im IOW
            IowKitFacade.Write(_device.Handler, Defines.PipeNumberSpecialMode, report, Defines.SpecialReportSize);
            if (Defines.SpecialReportSize == _device.IoReportsSize) {
                _device.AddCritical("Nicht möglich den I2C Modus zu initialisieren");
                return false;
            }

            // Pins für IOW Initaliseren
            var write = new byte[Defines.IoReportSize];
            write[0] = 0x00;
            write[1] = 0x00;
            write[2] = 0xF0;
            var writebyts = IowKitFacade.Write(_device.Handler, Defines.PipeNumberIo, write, _device.IoReportsSize);
            if (writebyts == _device.IoReportsSize) _device.AddEventLog("Pins wurden mit Default Werten initialisieren");

            // Debug LEDs einschalten
            _device.SetBit(0, 6, true);
            _device.SetBit(0, 7, true);

            // Setzten der Ports für die ein/aus funktion der ansteuerung. 
            // umgekerte Logik wegen der PNP Transistoren
            _device.SetBit(1, 4, true);
            _device.SetBit(1, 5, true);
            _device.SetBit(1, 6, true);
            _device.SetBit(1, 7, true);

            return true;
        }

        /// <summary>
        ///     Schreibt auf das IOw
        /// </summary>
        /// <param name="ports"></param>
        /// <returns></returns>
        public bool Write(Dictionary<int, Port> ports) {
            _writeIoRetryCounter = 0;
            WriteIo(ports);

            return ProcessDataQueue();
        }

        /// <summary>
        ///     Lesen der Ports
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, Port> Read() {
            // Beim ersten durchlauf muss einmal mit ReadDeviceImmediate 
            // gelesen werden damit ReadNonBlocking geht
            _readIoRetryCounter = 0;
            if (!_readFirst) {
                ReadDeviceImmediate();
                _readFirst = true;
            }

            var data = ReadNonBlocking();
            if (data != null) _device.SetDataReadStateToPort(data);

            return _device.Ports;
        }

        /// <summary>
        ///     Setzten der Timeouts
        /// </summary>
        /// <param name="readTimeout"></param>
        /// <returns></returns>
        public bool ReadTimeout(int readTimeout) {
            return true;
        }

        /// <summary>
        ///     Schreib den angestrebten zustand auf die I/O pins
        /// </summary>
        /// <param name="ports"></param>
        private void WriteIo(Dictionary<int, Port> ports) {
            var data = new byte[_device.IoReportsSize];
            data[0] = 0x00;

            foreach (var kvp in ports) {
                var p = kvp.Value;
                data[kvp.Key + Port.PortOffset] = p.GetBitStateAsByte();
            }

            var size = IowKitFacade.Write(_device.Handler, 0, data, _device.IoReportsSize);

            if (size == _device.IoReportsSize) return;

            if (_writeIoRetryCounter < Retrys) {
                _writeIoRetryCounter++;
                WriteIo(ports);
            } else {
                _device.AddCritical("Schreiben auf den I/O Port Fehlerhaft. Vorgang abgebrochen.");
            }
        }

        /// <summary>
        ///     Abarbeiten der Datenschleife
        /// </summary>
        /// <returns></returns>
        private bool ProcessDataQueue() {
            _removeList = new List<I2CData>();
            WriteDataQueue();
            RemoveAllDoneElements();
            return true;
        }

        /// <summary>
        ///     Bearbeitet alle Elemente in der Warte Schleife
        /// </summary>
        private void WriteDataQueue() {
            // Durch die Warteschleife der Daten gehen
            foreach (var i2CData in _dataQueue.ToList()) {
                _writei2CRetryCounter = 0;
                var result = WriteSingelRegister(i2CData.Adress, i2CData.Register, i2CData.Value);
                if (!result) _device.AddCritical("Schreiben auf den I2C Bus gescheitert Vorgang abgebrochen.");
                _removeList.Add(i2CData);
            }
        }

        /// <summary>
        ///     Entfernt alle Bearbeiteten Elemente aus der Warteschleife
        /// </summary>
        private void RemoveAllDoneElements() {
            // Alle geschriebenen Elemente aus der Warteschleife entfernen
            foreach (var i2CData in _removeList.ToList())
                try {
                    _dataQueue.Remove(i2CData);
                } catch (Exception) {
                    _device.Log.AddErrorLog(_device,
                        "Ein geschriebenes Element konnte nicht aus der Warteschleife entfernt werden.");
                    _dataQueue.Clear();
                }
        }


        /// <summary>
        ///     Schreiben eines Register im I2C Bus
        /// </summary>
        /// <param name="adress">IC2 Zieladresse</param>
        /// <param name="register">Zielregister auf der Adresse</param>
        /// <param name="value">Wert</param>
        /// <returns></returns>
        private bool WriteSingelRegister(byte adress, byte register, byte value) {
            var report = new byte[Defines.SpecialReportSize];

            // I2C Write
            report[0] = 0X02;

            // 3 Byte Start und Stop
            report[1] = 0XC3;

            report[2] = adress;
            report[3] = register;
            report[4] = value;


            IowKitFacade.Write(_device.Handler, Defines.PipeNumberSpecialMode, report, Defines.SpecialReportSize);
            IowKitFacade.Read(_device.Handler, Defines.PipeNumberSpecialMode, report, Defines.SpecialReportSize);

            // Im fehlerfall enthält report[1] eine 128
            if (report[1] != 128) return true;


            if (_writei2CRetryCounter < Retrys) {
                _writei2CRetryCounter++;
                return WriteSingelRegister(adress, register, value);
            }
            return false;
        }

        /// <summary>
        ///     Fügt eine neuen Befehlsatz in die Befehls Queue ein
        /// </summary>
        /// <param name="adress">Adresse auf dem I2C Bus</param>
        /// <param name="register">Register im I2C Baustein</param>
        /// <param name="value">Wert für das Register</param>
        public void AddDataToQueue(byte adress, byte register, byte value) {
            _dataQueue.Add(I2CData.Create(adress, register, value));
            _device.TriggerDataWrite();
        }

        /// <summary>
        ///     Liest den aktuellen Zustand der Ports aus.
        /// </summary>
        /// <returns></returns>
        private byte[] ReadDeviceImmediate() {
            var data = new byte[_device.ReportSize];

            var result = IowKitFacade.ReadImmediate(_device.Handler, data);

            if (result) return data;

            if (_readIoRetryCounter < Retrys) {
                _readIoRetryCounter++;
                return ReadDeviceImmediate();
            }
            _device.AddCritical("Lesen des Devices gescheitert");
            return data;
        }

        /// <summary>
        ///     Liest den aktuellen Zustand der Ports aus.
        /// </summary>
        /// <returns>null wenn keine Neuen daten im Stack vorliegen</returns>
        private byte[] ReadNonBlocking() {
            try {
                var data = new byte[_device.ReportSize];

                var result = IowKitFacade.IowKitReadNonBlocking(_device.Handler, Defines.PipeNumberIo, ref data,
                    _device.IoReportsSize);
                if (result == null) return null;
                return data;
            } catch (IowlibraryException e) {
                _device.AddCritical(e.Message);
                return null;
            }
        }
    }


    /// <summary>
    ///     Datenmodel zu abbildung der I2C Daten
    /// </summary>
    internal class I2CData {
        private I2CData(byte adress, byte register, byte value) {
            Adress = adress;
            Register = register;
            Value = value;
        }

        public byte Adress { get; set; }
        public byte Register { get; set; }
        public byte Value { get; set; }

        public static I2CData Create(byte adress, byte register, byte value) {
            return new I2CData(adress, register, value);
        }
    }
}