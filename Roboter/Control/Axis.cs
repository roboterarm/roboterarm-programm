﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using IowLibrary.Devices.Model;
using Roboter.Propertys;

namespace Roboter.Control {
    public class Axis {
        // Converter wert für die 12Bit Register
        private const double Converter = 4096D;

        // Zusätzliche Reservezeit beim Bewegen der Achse
        // Wird benötigt wegen der Zeit die der IOW brauch und der I2C
        // IOW Zyklus = 10ms I2C = 16ms*2
        private const long AdditionMoveTime = 42L;

        private readonly IAxisProperty _axisProperty;
        private readonly Device _device;
        private readonly byte _i2CAddrs;
        private readonly RobotMode _robotMode;

        private bool _isActive;

        // letzte Position der Achse die Angefahren worden is
        private int _lastPosition;
        private byte _registerOffH;
        private byte _registerOffL;
        private byte _registerOnH;

        private byte _registerOnL;
        private int _value;

        /// <summary>
        ///     Neue Instanz des PMW Moduls
        /// </summary>
        /// <param name="device">Device welches genutzt werden soll</param>
        /// <param name="i2CAddrs">I2C Adresse auf dem Bus</param>
        /// <param name="axisNumber">Nummer der Achse am Arm. Ist 0 Basierend</param>
        /// <param name="axisProperty">Achsen Einstellungen als Interface Type</param>
        public Axis(Device device, byte i2CAddrs, int axisNumber, IAxisProperty axisProperty) {
            AxisNumber = axisNumber;
            _i2CAddrs = i2CAddrs;
            _device = device;

            var robotMode = device.Mode as RobotMode;
            if (robotMode != null) _robotMode = robotMode;
            else device.AddCritical("Device ist nicht im I2C Mode");
            _axisProperty = axisProperty;
        }

        private int Min => CalcRegisterValue(_axisProperty.PulsMin());

        private int Max => CalcRegisterValue(_axisProperty.PulsMax());

        public int MaxPulse => _axisProperty.PulsMax();
        public int MinPulse => _axisProperty.PulsMin();

        public int MaxUser => _axisProperty.MaxUser();
        public int MinUser => _axisProperty.MinUser();

        /// <summary>
        ///     Positions wert der Achse
        /// </summary>
        public int Value { get; set; }

        /// <summary>
        ///     Achsennummer der Achse
        /// </summary>
        public int AxisNumber { get; set; }

        /// <summary>
        ///     Active status der Achse
        /// </summary>
        public bool IsActive {
            get { return _isActive; }

            set {
                _isActive = value;
                ChangeActiveStatus();
            }
        }

        public bool IsOverloadPosition { get; set; }

        /// <summary>
        ///     Geschwindigkeits wert mit die Achse verstellt wird in Anlauf und Ablauf
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        ///     Rechnent den wert um den die Achse sich bewegen soll und übergibt diesen an die I2C schnittstelle
        /// </summary>
        /// <param name="value">Wert um den die Achse verstellt werden soll</param>
        public long Move(int value) {
            _lastPosition = _value;
            _value = value;
            var calc = CalcRealRegisterValue(value);

            WriteToI2C(0, calc);

            return CalcMoveTime();
        }

        private int CalcRealRegisterValue(int value) {
            var range = Max - Min;
            var calc = (int) ((double) range/_axisProperty.MaxUser()*value) + Min;
            return calc;
        }

        /// <summary>
        ///     Setzten eines Min Values in der Achse
        /// </summary>
        /// <param name="min">neuer Minimal wert auf Basis der User Eingabe</param>
        /// <param name="oldMaxPuls">alter Maximalpuls wird zum berechnen des Werts benötigt</param>
        public void ChangeMin(int min, int oldMaxPuls) {
            var minPuls = _axisProperty.PulsMin();
            var range = oldMaxPuls - minPuls;
            var steps = range/_axisProperty.MaxUser();
            var newMinPuls = steps*min;

            _axisProperty.SetPulsMin(newMinPuls);
        }

        /// <summary>
        ///     Setzten eines Max Values in der Achse
        /// </summary>
        /// <param name="max">neuer Maximalwert auf Basis der User Eingabe</param>
        /// <param name="oldMinPuls">aler MinPuls Value wird benötigt für das berechnen des werts.</param>
        public void ChangeMax(int max, int oldMinPuls) {
            var maxPuls = _axisProperty.PulsMax();
            var range = maxPuls - oldMinPuls;
            var steps = range/_axisProperty.MaxUser();
            var newMinPuls = steps*max;

            _axisProperty.SetPulsMax(newMinPuls);
        }

        // Schreiben der Werte auf den I2C
        private void WriteToI2C(int on, int off) {
            CalcServoAdress(AxisNumber);

            // Erzeugen der oberen unter unteren Byte für die Kontrollregister
            var on1 = (byte) (on & 0xff);
            var on2 = (byte) (on >> 8);
            var off1 = (byte) (off & 0xff);
            var off2 = (byte) (off >> 8);

            // Fügt die Achsen zur warteschleife des I2C modus hinzu
            _robotMode.AddDataToQueue(_i2CAddrs, _registerOnL, on1);
            _robotMode.AddDataToQueue(_i2CAddrs, _registerOnH, on2);
            _robotMode.AddDataToQueue(_i2CAddrs, _registerOffL, off1);
            _robotMode.AddDataToQueue(_i2CAddrs, _registerOffH, off2);
        }

        // Berechnung der Kontrollregister
        private void CalcServoAdress(int servoNumber) {
            _registerOnL = (byte) (servoNumber*4 + 6);
            _registerOnH = (byte) (servoNumber*4 + 7);
            _registerOffL = (byte) (servoNumber*4 + 8);
            _registerOffH = (byte) (servoNumber*4 + 9);
        }

        private int CalcRegisterValue(int value) {
            // umrechnen in µs
            double ns = _axisProperty.Propertys().DutyTime*1000;

            return (int) (Converter/ns*value);
        }

        private void ChangeActiveStatus() {
            _device.SetBit(1, AxisNumber + 4, _isActive);
            _device.TriggerDataWrite();
        }

        private long CalcMoveTime() {
            double speed = _axisProperty.Speed();
            double actualMoveSpan;
            if (_lastPosition > _value) actualMoveSpan = _lastPosition - _value;
            else actualMoveSpan = _value - _lastPosition;

            var moveRange = Max - Min;

            var moveTime = speed/moveRange*actualMoveSpan + AdditionMoveTime;

            if (moveTime <= 0) return AdditionMoveTime;

            return (long) moveTime;
        }


        public override string ToString() {
            return "Achse: " + AxisNumber + " Value: " + Value;
        }
    }
}