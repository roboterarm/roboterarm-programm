﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Threading;
using IowLibrary.Devices.Control;
using IowLibrary.Devices.Model;
using Roboter.Propertys;
using Roboter.scriptcode;

namespace Roboter.Control {
    /// <summary>
    ///     Robeter Events
    /// </summary>
    public delegate void RoboterEvents(string msg);


    /// <summary>
    ///     Facade Klasse für die Roboter Steuerung
    /// </summary>
    public class RobotFacade {
        private readonly Compiler _compiler = new Compiler();
        private readonly DeviceRuntimeEventHandler _deviceFactoryRunTimeRuntime;

        private readonly DeviceManager _deviceManager;
        private readonly DevicPortEventHandler _devicePortBitChange;
        private readonly GlobalProperty _globalProperty;
        private AxisController _axisController;
        private Device _device;
        private Runtime _runtime;

        /// <summary>
        ///     Instanzierung der FacedKlasse
        /// </summary>
        /// <param name="deviceError">Device Error Events</param>
        /// <param name="deviceEvent">Device Eventes</param>
        /// <param name="devicePortBitChange">Ports Change Events</param>
        /// <param name="deviceFactoryRunTimeRuntime">Runtime Update</param>
        public RobotFacade(ManagerEventHandler deviceError, ManagerEventHandler deviceEvent,
            DevicPortEventHandler devicePortBitChange, DeviceRuntimeEventHandler deviceFactoryRunTimeRuntime) {
            _globalProperty = GlobalProperty.GetInstance();

            _deviceManager = new DeviceManager(deviceEvent, deviceEvent, deviceEvent, deviceError);
            _devicePortBitChange = devicePortBitChange;
            _deviceFactoryRunTimeRuntime = deviceFactoryRunTimeRuntime;

            _compiler.EventCompilerDone += _compiler_EventCompilerDone;
        }

        /// <summary>
        ///     Wird aufgerufen wenn der Compiler fertig ist.
        /// </summary>
        public event RoboterEvents CompilerDone;

        /// <summary>
        ///     Wird aufgerufen die Calibrirung abgeschlossen
        /// </summary>
        public event RoboterEvents CalibrationDone;

        /// <summary>
        ///     Wird aufgerufen die Calibrirung abgeschlossen
        /// </summary>
        public event RoboterEvents RuntimeProgress;

        /// <summary>
        ///     Wird aufgerufen die Calibrirung abgeschlossen
        /// </summary>
        public event RoboterEvents RuntimeDone;

        /// <summary>
        ///     Abrufen der Compiler Messages
        /// </summary>
        /// <returns></returns>
        public IList<CompileMessage> GetCompilerMessages() {
            return _compiler.CompileMessages;
        }

        /// <summary>
        ///     Gibt eine Liste aller verbundenen Devices zurück
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, Device> GetConnectedDevice() {
            return _deviceManager.Devices;
        }

        /// <summary>
        ///     Connection zum IOW Herstellen
        /// </summary>
        /// <returns>true im falle eine erfolgs</returns>
        public bool ConnectIow() {
            var isConnected = _deviceManager.InitFactory();

            if (isConnected) {
                _device = _deviceManager.GetDeviceNumber(1);
                _deviceManager.RunDevice(1, _devicePortBitChange, _deviceFactoryRunTimeRuntime, new RobotMode());

                _axisController = new AxisController(_device, _globalProperty.Propertys.Pmwi2CAdresse,
                    _globalProperty.Propertys.DutyTime);
                _axisController.AddAxis(1, _globalProperty.Axis1);
                _axisController.AddAxis(2, _globalProperty.Axis2);
                _axisController.AddAxis(3, _globalProperty.Axis3);
                _axisController.AddAxis(4, _globalProperty.Axis4);

                _axisController.CalibrationDone += _axisController_CalibrationDone;
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Verbindungen zum IOW Schließen
        /// </summary>
        /// <returns></returns>
        public bool DisconnectIow() {
            if (_axisController != null) {
                _axisController.ChangeActive(1, true);
                _axisController.ChangeActive(2, true);
                _axisController.ChangeActive(3, true);
                _axisController.ChangeActive(4, true);

                // TODO ist nicht ganz sauber besser wäre ein Progressbar...
                Thread.Sleep(400);
            }

            _deviceManager.RemoveAllDevices();
            return true;
        }

        /// <summary>
        ///     Compilieren von ScriptCode
        /// </summary>
        /// <param name="sourceCode"></param>
        public void Compile(string sourceCode) {
            _compiler.Compile(sourceCode);
        }

        /// <summary>
        ///     Laufen lassen des Compilerten ScripteCodes
        /// </summary>
        public void RunProgramm() {
            _runtime = new Runtime(_axisController) {Commands = _compiler.Commands};
            _runtime.Progress += _runtime_Progress;
            _runtime.Done += _runtime_Done;
            _runtime.Run();
        }

        /// <summary>
        ///     Abbrechen der Runtimer
        /// </summary>
        public void CancelProgramm() {
            _runtime?.Cancel();
        }

        /// <summary>
        ///     Ändert den Aktive Status der Achse
        /// </summary>
        /// <param name="number">Nummer der Achse</param>
        /// <param name="active"></param>
        public void ChangeActiveAxis(int number, bool active) {
            _axisController.ChangeActive(number, active);
        }

        /// <summary>
        ///     Bewegt eine Achse
        /// </summary>
        /// <param name="number">Nummer der Achse</param>
        /// <param name="value">wert um den die Achse verstellt werden soll</param>
        public void MoveAxis(int number, int value) {
            _axisController.MoveAxis(number, value);
        }

        /// <summary>
        ///     Geschwindigkeit der Achse
        /// </summary>
        /// <param name="number">Nummer der Achse</param>
        /// <param name="value">wert </param>
        public void ChangeSpeed(int number, int value) {
            _axisController.ChangeSpeed(number, value);
        }

        /// <summary>
        ///     Rückgabe des Aktuellen Port Statuses von Port1
        /// </summary>
        /// <returns></returns>
        public Port GetPort1State() {
            return _device?.Ports[1];
        }

        /// <summary>
        ///     Starte die Kalibierung der Achsen
        /// </summary>
        public void StartCalibration() {
            _axisController?.StartCalibration();
        }

        /// <summary>
        ///     Bricht die Kalibierung der Achsen ab
        /// </summary>
        public void CancelCalibration() {
            _axisController?.CancelCalibration();
        }

        private void _axisController_CalibrationDone() {
            CalibrationDone?.Invoke(null);
        }

        private void _compiler_EventCompilerDone() {
            CompilerDone?.Invoke(null);
        }

        private void _runtime_Done(string msg) {
            RuntimeDone?.Invoke(msg);
        }

        private void _runtime_Progress(string msg) {
            RuntimeProgress?.Invoke(msg);
        }
    }
}