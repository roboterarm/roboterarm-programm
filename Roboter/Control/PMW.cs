﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using IowLibrary.Devices.Model;

namespace Roboter.Control {
    /// <summary>
    ///     Steuerung von PMW Modulen über I2C
    /// </summary>
    public class Pmw {
        private readonly byte _i2CAddrs;
        private readonly RobotMode _robotMode;

        private byte _prescale;

        /// <summary>
        ///     Neue Instanz des PMW Moduls
        /// </summary>
        /// <param name="device">Device welches genutzt werden soll</param>
        /// <param name="i2CAdress">I2C Adresse auf dem Bus</param>
        public Pmw(Device device, byte i2CAdress) {
            var device1 = device;
            _i2CAddrs = i2CAdress;
            var robotMode = device1.Mode as RobotMode;
            if (robotMode != null) _robotMode = robotMode;
            else device1.AddEventLog("Device ist nicht im I2C Mode");
        }

        /// <summary>
        ///     Set the Frequenz of the PMW signal
        ///     <param name="dutyTime">Zykluszeit in ms</param>
        /// </summary>
        public void SetFrequenz(int dutyTime) {
            // umrechnen in herz
            var herz = 1000D/dutyTime;
            _prescale = CalcPrescale(herz);

            _robotMode.AddDataToQueue(_i2CAddrs, 0x00, 0x10);
            _robotMode.AddDataToQueue(_i2CAddrs, 0xfe, _prescale);
            _robotMode.AddDataToQueue(_i2CAddrs, 0x00, 0x00);

            // sleep for 10ms 
            _robotMode.AddDataToQueue(_i2CAddrs, 0x00, 0x80);
        }

        private static byte CalcPrescale(double frequenz) {
            //int prescale = (25Mhz / (4096 * dutyTime)) - 1
            // durch das convertieren zu int wird automatisch abgerundet
            return (byte) (25000000D/(4096D*frequenz) - 1);
        }
    }
}