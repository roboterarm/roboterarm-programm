﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Collections.Generic;
using System.Threading;
using IowLibrary.Devices.Model;
using Roboter.Propertys;

namespace Roboter.Control {
    /// <summary>
    ///     Events die durch den AxisController ausgelöst werden können
    /// </summary>
    public delegate void AxisControllerEvent();

    /// <summary>
    ///     Events die von Achsen verursacht werden können
    /// </summary>
    /// <param name="axisNumber"></param>
    /// <param name="state"></param>
    public delegate void AxisEvent(int axisNumber, bool state);

    public delegate void CalibrationDoneCallback();

    /// <summary>
    ///     Ist für die Steuerung und die Kontrolle der Achsen verantwortlich.
    /// </summary>
    public class AxisController {
        private readonly Dictionary<int, Axis> _axis = new Dictionary<int, Axis>();
        private readonly int _dutyTime;

        private readonly byte _i2CAdress;
        private readonly Pmw _pmw;

        public readonly Device Device;
        private Calibration _calibration;

        /// <summary>
        ///     Erzeugt eine neue instanz des Achsen Controller
        /// </summary>
        /// <param name="device">Iow device</param>
        /// <param name="i2CAdress">I2C Adresse des Pmw moduls</param>
        /// <param name="dutyTime">Zykluszeit des PMW singnals in ms</param>
        public AxisController(Device device, int i2CAdress, int dutyTime) {
            Device = device;
            _i2CAdress = (byte) i2CAdress;

            // Erzeugen des PMW Moduls und schreiben der PMW werts über die I2C Adresse
            _pmw = new Pmw(Device, _i2CAdress);
            _dutyTime = dutyTime;
            _pmw.SetFrequenz(_dutyTime);

            Device.PortBitInChange += _device_PortBitInChange;
        }

        /// <summary>
        ///     Wird aufgerufen wenn die Kalibierung Abgeschlossen ist.
        /// </summary>
        public event AxisControllerEvent CalibrationDone;

        /// <summary>
        ///     OverloadPosition erreicht
        /// </summary>
        public event AxisEvent OverLoadPositionEvent;

        /// <summary>
        ///     Fügt eine neues Achse in den Controller ein.
        /// </summary>
        /// <param name="axisNumber">Die Achsenummer. Ist 1 basierend</param>
        /// <param name="axisProperty">Achsen Settings aus der GlobalProperty</param>
        public
            void AddAxis(int axisNumber, IAxisProperty axisProperty) {
            axisNumber = axisNumber - 1;
            var axis = new Axis(Device, _i2CAdress, axisNumber, axisProperty);
            _axis.Add(axisNumber, axis);
            Device.AddEventLog("Achse " + axisNumber + " erfolgreich eingefügt");
        }

        /// <summary>
        ///     Gibt alle Achsen des Roboters zurück
        /// </summary>
        /// <returns>Ein Dictonary mit allen Achsen</returns>
        public IDictionary<int, Axis> GetAllRegistertAxis() {
            return _axis;
        }

        /// <summary>
        ///     Gibt eine Achse als object zurück
        /// </summary>
        /// <param name="axisNumber">Nummer der Achse. Ist 1 basierend</param>
        /// <returns>die Achse, wenn nicht vorhanden null</returns>
        internal Axis GetAxis(int axisNumber) {
            axisNumber = axisNumber - 1;
            Axis axis;
            _axis.TryGetValue(axisNumber, out axis);
            return axis;
        }

        /// <summary>
        ///     Bewegt eine Achse zum gewählten Value
        /// </summary>
        /// <param name="axisNumber">Nummer des Achse die Bewegt werden soll. Ist 1 basierend</param>
        /// <param name="value">Wert um den die Achse bewegt werden soll</param>
        /// <returns>Berechnete Zeit die gebraucht wird um diese Achse zu verstellen</returns>
        public long MoveAxis(int axisNumber, int value) {
            var axis = GetAxis(axisNumber);
            var move = axis?.Move(value);

            if (move != null) return (long) move;

            return 0;
        }

        /// <summary>
        ///     Ändert die Achsengeschwindigkeit an einer Achse
        /// </summary>
        /// <param name="axisNumber">Nummer des Achse die Bewegt werden soll. Ist 1 basierend</param>
        /// <param name="speed">Geschwindigkeit mit dem die Achse bewegt werden soll</param>
        public void ChangeSpeed(int axisNumber, int speed) {
            var axis = GetAxis(axisNumber);
            if (axis == null) return;
            axis.Speed = speed;
        }

        /// <summary>
        ///     Ändert den Active Status der Achse
        /// </summary>
        /// <param name="axisNumber">Nummer des Achse die Bewegt werden soll. Ist 1 basierend</param>
        /// <param name="isActive">bool wert der den Active Status der Achse abbildet</param>
        public void ChangeActive(int axisNumber, bool isActive) {
            var axis = GetAxis(axisNumber);
            if (axis == null) return;
            axis.IsActive = isActive;
        }

        /// <summary>
        ///     Setzt in einer Achsen den Overload Status
        /// </summary>
        /// <param name="axisNumber">Nummer des Achse die Bewegt werden soll. Ist 1 basierend</param>
        /// <param name="state"></param>
        public void SetAxisOverload(int axisNumber, bool state) {
            var axis = GetAxis(axisNumber);
            if (axis == null) return;
            axis.IsOverloadPosition = state;
        }

        /// <summary>
        ///     Fragt in einer Achsen den OverLoad Status ab
        /// </summary>
        /// <param name="axisNumber">Nummer des Achse die Bewegt werden soll. Ist 1 basierend</param>
        /// <returns></returns>
        public bool? GetAxisOverload(int axisNumber) {
            var axis = GetAxis(axisNumber);
            return axis?.IsOverloadPosition;
        }

        /// <summary>
        ///     Ändert den minimal wert einer Achse
        /// </summary>
        /// <param name="axisNumber"></param>
        /// <param name="min"></param>
        /// <param name="oldMaxPuls"></param>
        public void ChangeAxisMinValue(int axisNumber, int min, int oldMaxPuls) {
            var axis = GetAxis(axisNumber);
            axis.ChangeMin(min, oldMaxPuls);
        }

        /// <summary>
        ///     Ändert den maximal wert einer Achse
        /// </summary>
        /// <param name="axisNumber"></param>
        /// <param name="max"></param>
        /// <param name="oldMinPuls"></param>
        public void ChangeAxisMaxValue(int axisNumber, int max, int oldMinPuls) {
            var axis = GetAxis(axisNumber);
            axis.ChangeMax(max, oldMinPuls);
        }

        /// <summary>
        ///     Startet die Kalibierung der Achsen
        /// </summary>
        public void StartCalibration() {
            _calibration = new Calibration(this) {CalibrationDoneCallback = CalibrationDoneCallback};
            var workerThread = new Thread(_calibration.DoWork);

            workerThread.Start();
            Device.AddEventLog("Kalibierung der Achsen gestartet");
        }

        /// <summary>
        ///     Bricht die Kalibierung der Achsen ab.
        /// </summary>
        public void CancelCalibration() {
            _calibration?.RequestStop();
        }

        private void CalibrationDoneCallback() {
            CalibrationDone?.Invoke();
            Device.AddEventLog("Kalibierung der Achsen abgeschlossen");
            _pmw.SetFrequenz(_dutyTime);
        }

        private void _device_PortBitInChange(Device device, Port port, PortBit portBit) {
            if (port.PortNumber != 1) return;

            switch (portBit.BitNumber) {
                case 0:
                    OverLoadPosition(1, portBit.BitIn);
                    break;
                case 1:
                    OverLoadPosition(2, portBit.BitIn);
                    break;
                case 2:
                    OverLoadPosition(3, portBit.BitIn);
                    break;
                case 3:
                    OverLoadPosition(4, portBit.BitIn);
                    break;
            }
        }

        private void OverLoadPosition(int axisNumber, bool state) {
            SetAxisOverload(axisNumber, state);
            OverLoadPositionEvent?.Invoke(axisNumber, state);
        }
    }
}