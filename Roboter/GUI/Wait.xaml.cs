﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System.Windows;

namespace Roboter.GUI {
    public delegate void WaitEvent();

    /// <summary>
    ///     Interaction logic for Wait.xaml
    /// </summary>
    public partial class Wait {
        public Wait() {
            InitializeComponent();
        }

        /// <summary>
        ///     Setzen einer Custom Nachricht ins Dialog
        /// </summary>
        public string Msg {
            set { LabelMsg.Content = value; }
        }

        /// <summary>
        ///     Setzten einer Custom Progress Msg ins Dialog
        /// </summary>
        public string ProgressMsg {
            set { LabelProgressMsg.Content = value; }
        }

        /// <summary>
        ///     Wenn der Nutzer den Vorgang Manuell Abbrechen will
        /// </summary>
        public event WaitEvent CancelUser;

        private void Cancel_Click(object sender, RoutedEventArgs e) {
            CancelUser?.Invoke();

            Close();
        }
    }
}