﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Forms;
using System.Xml;
using ICSharpCode.AvalonEdit.Highlighting;
using ICSharpCode.AvalonEdit.Highlighting.Xshd;
using Roboter.scriptcode;
using MessageBox = System.Windows.MessageBox;

namespace Roboter.GUI {
    /// <summary>
    ///     Menü Events im SourceCodeEditor Editor
    /// </summary>
    public delegate void SourceCodeMenuEvent(string source);

    /// <summary>
    ///     Interaction logic for SourceCodeEditor.xaml
    /// </summary>
    public partial class SourceCodeEditor {
        private IList<CompileMessage> _compileMessages;

        private string _sourceCode;


        public SourceCodeEditor() {
            InitializeComponent();
        }

        /// <summary>
        ///     Setzen oder Lesen des eingebenen SourceCodes als String
        /// </summary>
        public string SourceCode {
            get {
                _sourceCode = TxtScourceCode.Text;
                return _sourceCode;
            }
            set {
                _sourceCode = value;
                TxtScourceCode.Text = _sourceCode;
            }
        }

        /// <summary>
        ///     Text ist verändert worden deshalb ist der SourceCode Dirty
        /// </summary>
        public bool IsDirty { get; private set; }

        public IList<CompileMessage> CompileMessages {
            set {
                _compileMessages = value;
                SetMessagesToGui();
            }
        }

        /// <summary>
        ///     Menü Auswahl Speichern des SourceCodes
        /// </summary>
        public event SourceCodeMenuEvent MenuSaveClick;

        /// <summary>
        ///     Menü Auwahl Laden des SourceCodes
        /// </summary>
        public event SourceCodeMenuEvent MenuLoadClick;

        /// <summary>
        ///     Menü auswahl Prüfen des SourceCodes
        /// </summary>
        public event SourceCodeMenuEvent MenuCheckClick;

        /// <summary>
        ///     Führt den SourceCodeEditor aus
        /// </summary>
        public event SourceCodeMenuEvent MenuRunClick;

        /// <summary>
        ///     Laden von QuellCode Datein
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuLoad_OnClick(object sender, RoutedEventArgs e) {
            MenuLoadClick?.Invoke(SourceCode);
            var openFileDialog = new OpenFileDialog {
                InitialDirectory = "c:\\",
                Filter = Properties.Resources.SourceCodeEditor_MenuLoad_OnClick_Robot_Script_Dateien,
                FilterIndex = 1,
                RestoreDirectory = true
            };

            if (openFileDialog.ShowDialog() == DialogResult.OK)
                try {
                    var myStream = openFileDialog.OpenFile();
                    using (myStream) {
                        // convert stream to string
                        var reader = new StreamReader(myStream);
                        SourceCode = reader.ReadToEnd();
                    }
                } catch (Exception ex) {
                    MessageBox.Show("Error: Konnte Datei nicht öffnen: " + ex.Message);
                }
        }

        /// <summary>
        ///     Speichern von Quellcode Dateien
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSave_OnClick(object sender, RoutedEventArgs e) {
            MenuSaveClick?.Invoke(SourceCode);
            SaveScripteCode();
        }

        /// <summary>
        ///     Öffnen des Speicher Dialoges
        /// </summary>
        public void SaveScripteCode() {
            var saveFileDialog = new SaveFileDialog {
                Filter = Properties.Resources.SourceCodeEditor_MenuLoad_OnClick_Robot_Script_Dateien,
                FilterIndex = 1,
                RestoreDirectory = true
            };


            if (saveFileDialog.ShowDialog() == DialogResult.OK) {
                File.WriteAllText(saveFileDialog.FileName, SourceCode);
                IsDirty = false;
            }
        }

        private void MenuCheckCode_OnClick(object sender, RoutedEventArgs e) {
            MenuCheckClick?.Invoke(SourceCode);
        }

        private void MenuRun_OnClick(object sender, RoutedEventArgs e) {
            MenuRunClick?.Invoke(SourceCode);
        }

        private void TxtScourceCode_TextChanged(object sender, EventArgs eventArgs) {
            IsDirty = true;
        }

        private void TxtScourceCode_Loaded(object sender, RoutedEventArgs e) {
            using (Stream s = new MemoryStream(Properties.Resources.RobotHighlight)) {
                using (var reader = new XmlTextReader(s)) {
                    TxtScourceCode.SyntaxHighlighting = HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }
        }

        public void AddMoveCommand(int axisNumber, int value) {
            var move = new Move(axisNumber, value);
            SourceCode = SourceCode + Environment.NewLine + move.ToScriptCode();
        }


        private void SetMessagesToGui() {
            if (SourceCodeMsg.Dispatcher.CheckAccess()) {
                SourceCodeMsg.Items.Clear();

                if (_compileMessages == null) return;
                foreach (var compileMessage in _compileMessages) SourceCodeMsg.Items.Add(compileMessage);
            } else {
                var slc = new SetCompileMsgCallback(SetMessagesToGui);
                Dispatcher.Invoke(slc);
            }
        }

        private delegate void SetCompileMsgCallback();
    }
}