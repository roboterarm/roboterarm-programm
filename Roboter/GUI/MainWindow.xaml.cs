﻿/*
* IowLibrary
* 
* Copyright © 2017 M. Vervoorst www.edlly.de software@edlly.de
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
* associated documentation files (the "Software"), to deal in the Software without restriction, 
* including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
* and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
* subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all copies or substantial 
* portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT 
* NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
* OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
* 
* https://opensource.org/licenses/mit-license.php
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using IowLibrary.Devices.Control;
using IowLibrary.Devices.Model;
using Roboter.Control;
using Roboter.Propertys;
using Roboter.Utils;

// TODO aufräumen und die Active Portchange in die Facade packen

namespace Roboter.GUI {
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow {
        private readonly ICollectionView _defaultView;
        private readonly GlobalProperty _globalProperty;

        private readonly ICollection<LogEntry> _logEntries = new List<LogEntry>();

        private readonly RobotFacade _robotFacade;
        private Wait _wait;


        public MainWindow() {
            InitializeComponent();

            _robotFacade = new RobotFacade(DeviceManager_Error, DeviceManger_EventLog, Device_PortChangeStatus,
                DeviceFactoryRunTimeUpdate);

            // instanz der Propertys erzeugen
            _globalProperty = GlobalProperty.GetInstance();

            SourceCode.MenuCheckClick += SourceCode_MenuCheckClick;
            SourceCode.MenuRunClick += SourceCode_MenuRunClick;

            _robotFacade.CompilerDone += _robotFacade_CompilerDone;
            _robotFacade.CalibrationDone += _robotFacade_CalibrationDone;
            _robotFacade.RuntimeProgress += _robotFacade_RuntimeProgress;
            _robotFacade.RuntimeDone += _robotFacade_RuntimeDone;

            _defaultView = CollectionViewSource.GetDefaultView(_logEntries);
            ListLog.ItemsSource = _defaultView;


            AddAxis();
        }

        private void MenuConnectIow_Click(object sender, RoutedEventArgs e) {
            Connect();
        }

        private void MenuCloseIow_Click(object sender, RoutedEventArgs e) {
            Disconnect();
        }

        private void MenuClose_Click(object sender, RoutedEventArgs e) {
            Disconnect();
        }

        private bool Connect() {
            var isConnected = _robotFacade.ConnectIow();

            if (isConnected) {
                MenuCloseIow.IsEnabled = true;
                MenuConnectIow.IsEnabled = false;
                MenuAutoCalibration.IsEnabled = true;
                SetDeviceList(_robotFacade.GetConnectedDevice());

                EnableAxis();
                return true;
            }
            return false;
        }

        private bool Disconnect() {
            _robotFacade.DisconnectIow();

            Axis1.IsEnabled = false;
            Axis2.IsEnabled = false;
            Axis3.IsEnabled = false;
            Axis4.IsEnabled = false;

            MenuCloseIow.IsEnabled = false;
            MenuConnectIow.IsEnabled = true;
            SetDeviceList(null);
            return true;
        }

        /// <summary>
        ///     Einfügen der Achsen in die Oberfläche
        /// </summary>
        private void AddAxis() {
            Axis1.Propertys = _globalProperty.Axis1;
            Axis1.AxisValueChanged += Axis1OnAxisValueChanged;
            Axis1.SpeedValueChanged += Axis1_SpeedValueChanged;
            Axis1.OnChangedActive += Axis1OnChangedActive;
            Axis1.AddMoveToScript += Axis1_AddMoveToScript;

            Axis2.Propertys = _globalProperty.Axis2;
            Axis2.AxisValueChanged += Axis2OnAxisValueChanged;
            Axis2.SpeedValueChanged += Axis2_SpeedValueChanged;
            Axis2.OnChangedActive += Axis2_OnChangedActive;
            Axis2.AddMoveToScript += Axis2_AddMoveToScript;

            Axis3.Propertys = _globalProperty.Axis3;
            Axis3.AxisValueChanged += Axis3OnAxisValueChanged;
            Axis3.SpeedValueChanged += Axis3_SpeedValueChanged;
            Axis3.OnChangedActive += Axis3_OnChangedActive;
            Axis3.AddMoveToScript += Axis3_AddMoveToScript;

            Axis4.Propertys = _globalProperty.Axis4;
            Axis4.AxisValueChanged += Axis4OnAxisValueChanged;
            Axis4.SpeedValueChanged += Axis4_SpeedValueChanged;
            Axis4.OnChangedActive += Axis4_OnChangedActive;
            Axis4.AddMoveToScript += Axis4_AddMoveToScript;
        }

        private void EnableAxis() {
            Axis1.IsEnabled = true;
            Axis2.IsEnabled = true;
            Axis3.IsEnabled = true;
            Axis4.IsEnabled = true;
        }

        private void Axis1_AddMoveToScript(int value) {
            SourceCode.AddMoveCommand(1, value);
        }

        private void Axis2_AddMoveToScript(int value) {
            SourceCode.AddMoveCommand(2, value);
        }

        private void Axis3_AddMoveToScript(int value) {
            SourceCode.AddMoveCommand(3, value);
        }

        private void Axis4_AddMoveToScript(int value) {
            SourceCode.AddMoveCommand(4, value);
        }

        private void Axis1OnChangedActive(bool active) {
            _robotFacade.ChangeActiveAxis(1, active);
        }

        private void Axis2_OnChangedActive(bool active) {
            _robotFacade.ChangeActiveAxis(2, active);
        }

        private void Axis3_OnChangedActive(bool active) {
            _robotFacade.ChangeActiveAxis(3, active);
        }

        private void Axis4_OnChangedActive(bool active) {
            _robotFacade.ChangeActiveAxis(4, active);
        }

        private void Axis1OnAxisValueChanged(int value) {
            _robotFacade.MoveAxis(1, value);
        }

        private void Axis2OnAxisValueChanged(int value) {
            _robotFacade.MoveAxis(2, value);
        }

        private void Axis3OnAxisValueChanged(int value) {
            _robotFacade.MoveAxis(3, value);
        }

        private void Axis4OnAxisValueChanged(int value) {
            _robotFacade.MoveAxis(4, value);
        }

        private void Axis4_SpeedValueChanged(int value) {
            _robotFacade.ChangeSpeed(1, value);
        }

        private void Axis2_SpeedValueChanged(int value) {
            _robotFacade.ChangeSpeed(2, value);
        }

        private void Axis3_SpeedValueChanged(int value) {
            _robotFacade.ChangeSpeed(3, value);
        }

        private void Axis1_SpeedValueChanged(int value) {
            _robotFacade.ChangeSpeed(4, value);
        }


        private void SetLog(List<LogEntry> errorItem) {
            if (ListLog.Dispatcher.CheckAccess()) {
                foreach (var logEntry in errorItem) {
                    _logEntries.Add(logEntry);
                    _defaultView.Refresh();
                    AutoScrollDownLogList();
                }
            } else {
                var slc = new SetEventLogCallback(SetLog);
                Dispatcher.Invoke(slc, errorItem);
            }
        }

        private void SetDeviceList(Dictionary<int, Device> devices) {
            if (ListConnectedDevices.Dispatcher.CheckAccess()) {
                ListConnectedDevices.Items.Clear();

                if (devices == null) return;

                foreach (var device in devices) ListConnectedDevices.Items.Add(device.Value);
            } else {
                var slc = new SetDeviceConnectedCallback(SetDeviceList);
                Dispatcher.Invoke(slc, devices);
            }
        }

        private void DeviceFactoryRunTimeUpdate(long runtime) {
        }

        // TODO vermischt GUI und Controller sachen...
        private void Device_PortChangeStatus(Device device, Port port, PortBit portbit) {
            if (Dispatcher.CheckAccess()) {
                EndlageChanged(port, portbit);
            } else {
                var slc = new PortChangeCallback(Device_PortChangeStatus);
                Dispatcher.Invoke(slc, device, port, portbit);
            }
        }

        // TODO vermischt GUI und Controller sachen...
        private void EndlageChanged(Port port, PortBit portbit) {
            // Todo configuriebar machen!
            if (port.PortNumber == 1)
                switch (portbit.BitNumber) {
                    case 0:
                        Axis1.IsEndPosition = portbit.BitIn;
                        break;
                    case 1:
                        Axis2.IsEndPosition = portbit.BitIn;
                        break;
                    case 2:
                        Axis3.IsEndPosition = portbit.BitIn;
                        break;
                    case 3:
                        Axis4.IsEndPosition = portbit.BitIn;
                        break;
                    default:
                        RefreshActiveState();
                        break;
                }
        }

        private void RefreshActiveState() {
            var port = _robotFacade.GetPort1State();
            Axis1.IsActive = port.GetBitState(4);
            Axis2.IsActive = port.GetBitState(5);
            Axis3.IsActive = port.GetBitState(6);
            Axis4.IsActive = port.GetBitState(7);
        }

        private void MenuSetting_Click(object sender, RoutedEventArgs e) {
            var window = new Window {
                Title = "Einstellung",
                Content = new SettingsDialog(),
                Width = 600
            };
            window.Show();
        }

        private void MainWindows_Closing(object sender, CancelEventArgs e) {
            if (SourceCode.IsDirty) {
                var messageBoxResult =
                    MessageBox.Show(
                        "Es sind noch nicht alle Änderung im ScriptCode gespeichert worden. Sollen diese gespeichert werden?",
                        "ScriptCode Fenster Dreckig", MessageBoxButton.YesNo);
                if (messageBoxResult == MessageBoxResult.Yes) SourceCode.SaveScripteCode();
            }
            Disconnect();
        }

        private void SourceCode_MenuRunClick(string source) {
            _robotFacade.Compile(source);

            _robotFacade.RunProgramm();

            Overlay.Visibility = Visibility.Visible;
            _wait = new Wait {Owner = this};
            _wait.CancelUser += Wait_CancelUser;
            _wait.ProgressMsg = "Programm läuft...";

            _wait.ShowDialog();
        }

        private void SourceCode_MenuCheckClick(string source) {
            _robotFacade.Compile(source);
        }

        private void DeviceManager_Error(DeviceManager deviceError) {
            SetLog(deviceError.Log.GetLogEntrysCriticalAndReset());
        }

        private void DeviceManger_EventLog(DeviceManager deviceEvent) {
            SetLog(deviceEvent.Log.GetLogAllNoneCrtical());
        }

        private void FilterInfo_Checked(object sender, RoutedEventArgs e) {
            FilterLogList();
            AutoScrollDownLogList();
        }

        private void FilterInfo_Unchecked(object sender, RoutedEventArgs e) {
            FilterLogList();
            AutoScrollDownLogList();
        }

        private void AutoScroll_Checked(object sender, RoutedEventArgs e) {
            AutoScrollDownLogList();
        }

        /// <summary>
        ///     Filter Funktion für die LogListe
        /// </summary>
        private void FilterLogList() {
            // Filter auf false Starten damit wir belibig zusammenbauen können.
            var predicate = PredicateBuilder.False<object>();

            // zufügen der Elemente
            if ((FilterInfo?.IsChecked != null) && FilterInfo.IsChecked.Value)
                predicate = predicate.Or(item => ((LogEntry) item).Level.Equals(LogEntry.LogLevel.Info));

            if ((FilterEvent?.IsChecked != null) && FilterEvent.IsChecked.Value)
                predicate = predicate.Or(item => ((LogEntry) item).Level.Equals(LogEntry.LogLevel.Event));

            if ((FilterError?.IsChecked != null) && FilterError.IsChecked.Value)
                predicate = predicate.Or(item => ((LogEntry) item).Level.Equals(LogEntry.LogLevel.Error));

            if ((FilterCritical?.IsChecked != null) && FilterCritical.IsChecked.Value)
                predicate = predicate.Or(item => ((LogEntry) item).Level.Equals(LogEntry.LogLevel.Critical));

            // Compileren und Casten zu Predicate
            var func = predicate.Compile();
            Predicate<object> pred = t => func(t);

            // Ist irgendein Filter gesetzt?
            if (((FilterInfo?.IsChecked != null) && !FilterInfo.IsChecked.Value) ||
                ((FilterEvent?.IsChecked != null) && !FilterEvent.IsChecked.Value) ||
                ((FilterError?.IsChecked != null) && !FilterError.IsChecked.Value) ||
                ((FilterCritical?.IsChecked != null) && !FilterCritical.IsChecked.Value)) SetFilter(pred);
            else SetFilter(null);
        }

        private void SetFilter(Predicate<object> pred) {
            if (_defaultView != null) _defaultView.Filter = pred;
        }

        /// <summary>
        ///     AutoScroll für die ListLog
        /// </summary>
        private void AutoScrollDownLogList() {
            if ((AutoScroll?.IsChecked == null) || !AutoScroll.IsChecked.Value) return;
            if (ListLog.Items.Count <= 0) return;
            var border = VisualTreeHelper.GetChild(ListLog, 0) as Decorator;
            var scroll = border?.Child as ScrollViewer;
            scroll?.ScrollToEnd();
        }

        private void MenuAutoCalibration_OnClick(object sender, RoutedEventArgs e) {
            _robotFacade.StartCalibration();

            Overlay.Visibility = Visibility.Visible;
            _wait = new Wait {Owner = this};
            _wait.CancelUser += Wait_CancelUser;
            _wait.ProgressMsg = "Kalibierung läuft...";

            _wait.ShowDialog();
        }

        private void Wait_CancelUser() {
            _robotFacade.CancelCalibration();
            _robotFacade.CancelProgramm();
            Overlay.Visibility = Visibility.Collapsed;
        }

        private void _robotFacade_CalibrationDone(string msg) {
            if (_wait.Dispatcher.CheckAccess()) {
                _wait.Close();
                Overlay.Visibility = Visibility.Collapsed;
            } else {
                var scl = new DialogCallback(_robotFacade_CalibrationDone);
                Dispatcher.Invoke(scl, msg);
            }
        }

        private void _robotFacade_CompilerDone(string msg) {
            SourceCode.CompileMessages = _robotFacade.GetCompilerMessages();
        }


        private void _robotFacade_RuntimeDone(string msg) {
            if (_wait == null) {
                return;
            }
            if (_wait.Dispatcher.CheckAccess()) {
                _wait.Close();
                Overlay.Visibility = Visibility.Collapsed;
            } else {
                var scl = new DialogCallback(_robotFacade_RuntimeDone);
                Dispatcher.Invoke(scl, msg);
            }
        }

        private void _robotFacade_RuntimeProgress(string msg) {
            if (_wait == null) {
                return;
            }
            if (_wait.Dispatcher.CheckAccess()) {
                _wait.ProgressMsg = msg;
            } else {
                var scl = new DialogCallback(_robotFacade_RuntimeProgress);
                Dispatcher.Invoke(scl, msg);
            }
        }

        private void MenuAutoAbout_OnClick(object sender, RoutedEventArgs e) {
            // TODO als WPF element machen
            var about = new About {Owner = this};
            about.ShowDialog();
        }

        private void MenuHelp_OnClick(object sender, RoutedEventArgs e) {
            Process.Start("https://bitbucket.org/roboterarm/roboterarm-programm");
        }

        private delegate void SetEventLogCallback(List<LogEntry> ojc);

        private delegate void SetDeviceConnectedCallback(Dictionary<int, Device> ojc);

        private delegate void PortChangeCallback(Device device, Port port, PortBit portbit);

        private delegate void DialogCallback(string msg);
    }
}